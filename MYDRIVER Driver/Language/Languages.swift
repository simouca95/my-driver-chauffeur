//
//  Language.swift
//  Language
//
//  Created by Karizma Technology on 2/27/17.
//  Copyright © 2017 Karizma Technology. All rights reserved.
//

import Foundation

class Languages{
    
    class func currentLanguage() -> String{
        let def = UserDefaults.standard
        
        let langs = def.object(forKey: "AppleLanguages") as! NSArray
        let firstLang = langs.firstObject as! String
        
        return firstLang
    }
    
    
    class func setAppLanguage(_ lang: String){
        let def = UserDefaults.standard
        def.set([lang, currentLanguage()], forKey: "AppleLanguages")
        def.synchronize()
    }
}
