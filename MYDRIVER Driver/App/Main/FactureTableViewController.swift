//
//  FactureTableViewController.swift
//  MyDriver
//
//  Created by sami hazel on 5/9/19.
//  Copyright © 2019 MyDriverKw. All rights reserved.
//

import UIKit
import Cosmos
import CoreData
import Foundation
import SwiftyJSON
import FirebaseStorage

protocol TripDidFinish {
    func tripDidFinish()
}
class FactureTableViewController: UITableViewController {
    
    
    static let storyboardId = "FactureTableViewController"
    
    
    @IBOutlet weak var done: UIButton!
    @IBOutlet weak var waitingTime: UILabel!
    @IBOutlet weak var tripBaseFare: UILabel!
    @IBOutlet weak var tripDuration: UILabel!
    @IBOutlet weak var totalFare: UILabel!
    @IBOutlet weak var driverName: UILabel!
    @IBOutlet weak var waitingCost: UILabel!
    @IBOutlet weak var costPerMin: UILabel!
    @IBOutlet weak var driverImage: UIImageView!
    @IBOutlet weak var cashPayment: UIView!
    
    @IBOutlet weak var knetPayment: UIView!
    
    var currentCourse : Course!
    
    var prices : [Price]!
    
    var tripFinishedDelegate : TripDidFinish!
    
    var jsonCourseData : JSON!

    var cashSelected = false
    var didSelectPaymentMethod = false
    var tapCash : UITapGestureRecognizer!
    var tapKnet : UITapGestureRecognizer!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        let screenSize = UIScreen.main.bounds.size
        
        let popupWidth = screenSize.width - 40
        
        let popupHeight = screenSize.height - 100
        
        let popupSize = CGSize(width: popupWidth, height: popupHeight)
        
        self.contentSizeInPopup = popupSize
        //popupController?.backgroundView?.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(self.dismissPopup)))
        
        self.popupController?.containerView.layer.cornerRadius = 10
        self.popupController?.containerView.clipsToBounds = true
        self.popupController?.containerView.backgroundColor = UIColor.clear
        self.popupController?.hidesCloseButton = true
        self.popupController?.navigationBarHidden = true
        
        let tapCash = UITapGestureRecognizer(target: self, action: #selector(self.selectPaymentMethod(_:)))
        tapCash.numberOfTapsRequired = 1
        let tapKnet = UITapGestureRecognizer(target: self, action: #selector(self.selectPaymentMethod(_:)))
        tapKnet.numberOfTapsRequired = 1

        self.knetPayment.addGestureRecognizer(tapKnet)
        self.cashPayment.addGestureRecognizer(tapCash)
        
        fetchPrices()
        
        setCourseDetails()
    }
    
    @objc func selectPaymentMethod(_ sender : UITapGestureRecognizer){
        print("selectPaymentMethod tapped")

        if sender.state == .ended{
            self.didSelectPaymentMethod = true
            if let view = sender.view{
                if view.tag == 1{
                    self.cashSelected = true
                    self.cashPayment.backgroundColor = UIColor.init(hex: 0x0e8dcf)
                    self.knetPayment.backgroundColor = UIColor.init(hex: 0xD6D6D6)
                }else if view.tag == 2 {
                    self.cashSelected = false
                    self.knetPayment.backgroundColor = UIColor.init(hex: 0x0e8dcf)
                    self.cashPayment.backgroundColor = UIColor.init(hex: 0xD6D6D6)
                    
                }
            }
        }

    }
    
    func setCourseDetails() {
        
        
        if  let firstName =  jsonCourseData["firstName"].string , let lastName =  jsonCourseData["lastName"].string{
            driverName.text = "\(firstName) \(lastName)"
        }
        self.driverImage.borderWithCornder(raduis: driverImage.frame.height / 2, borderColor: UIColor.black.cgColor, borderWidth: 1)
        
        self.driverImage.image = UIImage(named: "profileRegestration")

        if jsonCourseData["pictureURL"].stringValue.isValidURL(){
            driverImage.sd_setImage(with: URL(string: jsonCourseData["pictureURL"].stringValue), completed: { (image, error, cache, url) in
                if error == nil {
                    self.driverImage.image = image
                }
            })
        }else if jsonCourseData["pictureURL"].stringValue.count > 0 {
            let storageRef = Storage.storage().reference()
            let profileImagesRef = storageRef.child("ProfilePictures/\(jsonCourseData["pictureURL"].stringValue)")
            
            // You can also access to download URL after upload.
            profileImagesRef.downloadURL { (url, error) in
                guard let downloadURL = url else {
                    // Uh-oh, an error occurred!
                    return
                }
                self.driverImage.sd_setImage(with: downloadURL, completed: { (image, error, cache, url) in
                    if error == nil {
                        self.driverImage.image = image
                    }
                })
                
            }
        }

        
        self.totalFare.text = "\(self.currentCourse.finalCost ?? 0) KWD"
        
        let tripDurationInSec = Int(self.currentCourse.finishedAt!.timeIntervalSince(self.currentCourse.startedAt!))
        
        self.tripDuration.text = String(format:"%02ih, %02i min, %02i sec", tripDurationInSec / 3600, tripDurationInSec % 3600 / 60, tripDurationInSec % 60)
        
        let tripPrice = self.prices.first(where: { (price) -> Bool in
            price.category == self.currentCourse.classCar
        })
        self.tripBaseFare.text = "\(tripPrice!.baseFare!) KWD"
        self.costPerMin.text = "\(tripPrice!.priceTenMin!) KWD"
        
        if let wt = self.currentCourse.waitingTime{
            var freeWT = 0
            var costWT : Double = 0
            for i in wt {
                
                freeWT += i.duration
                if i.duration > 300{//5 mn in seconds
                    costWT += Double(Double(i.duration - 300) / 60).rounded(.up)
                }
            }
            if freeWT >= 3600{
                self.waitingTime.text = String(format:"%02ih, %02i min, %02i sec", freeWT / 3600, freeWT % 3600 / 60, freeWT % 60)

            }else if freeWT >= 60{
                self.waitingTime.text = String(format:"%02i min, %02i sec",freeWT / 60, freeWT % 60)

            }else {
                self.waitingTime.text = String(format:"%02i sec", freeWT % 60)

            }
            
            self.waitingCost.text = "\(((costWT * tripPrice!.waintingCost) * 1000).rounded(.up) / 1000) KWD"
            
        }else{
            self.waitingTime.text = "00 sec"
            self.waitingCost.text = "0 KWD"
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func fetchPrices(){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        let fetchCountReq = NSFetchRequest<NSFetchRequestResult>(entityName: "Prices")
        do {
            let test = try context.fetch(fetchCountReq)
            self.prices = [Price]()
            for object in test as! [NSManagedObject]{
                let price = Price(category: object.value(forKey:"category") as? String,
                                  pricePerKm: object.value(forKey:"pricePerKm") as? Double,
                                  pricePerMin: object.value(forKey:"pricePerMin") as? Double,
                                  waintingCost: object.value(forKey:"waitingCost") as? Double,
                                  baseFare: object.value(forKey:"baseFare") as? Double,
                                  priceTenMin: object.value(forKey:"priceTenMin") as? Double)
                self.prices.append(price)
            }
        } catch  {
            print(error)
        }
    }
    
    
    @IBAction func done(_ sender: Any) {
        if didSelectPaymentMethod{
            if self.cashSelected{
                self.currentCourse.customerDidPay(with: "cash")
            }else{
                self.currentCourse.customerDidPay(with: "knet")
            }
            
            if self.navigationController != nil {
                self.navigationController?.dismiss(animated: true, completion: nil)
                
            }else {
                self.dismiss(animated: true, completion: nil)
                
            }
            tripFinishedDelegate.tripDidFinish()
        }else {
            self.cashPayment.shake()
            self.knetPayment.shake()
            self.showAlertView(isOneButton: true, title: "Select customer payment method", cancelButtonTitle: "Cancel", submitButtonTitle: "Cancel")
        }

        
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 9
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0 :
            return 100
        case 1:
            return 180
        case 8:
            return 80
        default:
            return 40
        }
    }
}
