//
//  MainViewController.swift
//  MyDriver
//
//  Created by sami hazel on 4/6/19.
//  Copyright © 2019 MyDriverKw. All rights reserved.
//

import UIKit
import STPopup
import GoogleMaps
import Alamofire
import SwiftyJSON
import FirebaseDatabase
import FirebaseStorage
import Toaster
import FirebaseAuth
import CoreData
import NotificationBannerSwift
import DropDown

struct Price {
    var category : String!
    var pricePerKm : Double!
    var pricePerMin : Double!
    var waintingCost : Double!
    var baseFare : Double!
    var priceTenMin : Double!
}

struct Configs {
    var trafficModel : String
}


class MainDriverViewController: UIViewController {
    static let storyboardId = "MainDriverViewController"
    
    
    private var locationManager : CLLocationManager!
    
    @IBOutlet weak var mapView: GMSMapView!
    
    var currentPosition : Position!
    
    var clientPosition : Position!
    
    var jsonCourseData : JSON!
    
    @IBOutlet weak var googleMapBtn: UIButton!
    @IBOutlet weak var directionChanged: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    
    @IBOutlet weak var floatingButtonsHeight: NSLayoutConstraint!
    @IBOutlet weak var floatingButtons: UIView!
    
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var destinationLabel: UILabel!
    
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var profilePicture: UIImageView!
    
    @IBOutlet weak var clientPicture: UIImageView!
    @IBOutlet weak var clientPhoneNumber: UILabel!
    @IBOutlet weak var clientName: UILabel!
    @IBOutlet weak var topClientViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var clientDetailsView: UIView!
    
    @IBOutlet weak var startStopButton: UIButton!
    
    @IBOutlet weak var onOff: UIButton!
    
    @IBOutlet weak var waitingButton: UIButton!
    
    @IBOutlet weak var bottomDriverViewConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var startTrip: UIButton!
    @IBOutlet weak var timerLabel: UILabel!
    
    var waitingTimer : Timer!
    var waitingTimerValue = 0
    
    var smsTimer : Timer!
    var smsTimerValue = 0
    
    
    
    var minHeaderHeight: CGFloat = 0
    var maxHeaderHeight: CGFloat = 160
    var floatingButtonsOpened = false
    var polyline : GMSPolyline!
    var roadDedtails : JSON!
    
    var clientPictureUrl : String!
    
    var driver : Driver!
    
    var currentCourse : Course!
    
    var prices : [Price]!
    var config : Configs!
    
    var refHandle : UInt!
    
    var tapDest : UITapGestureRecognizer!
    var tapAddress : UITapGestureRecognizer!
    
    let dropDown = DropDown()
    
    let subjects = ["Customer didn't present after waiting", "Accident","Resume trip"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // it waits the driver to accept trip from notificationVC
        NotificationCenter.default.addObserver(self, selector: #selector(self.didAcceptTrip(_:)), name: NSNotification.Name("did_accept_race"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.clientDidCancelRace), name: NSNotification.Name("race_canceled"), object: nil)
        
        
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.allowsBackgroundLocationUpdates = true
        
        locationManager.requestAlwaysAuthorization()
        
        //UserDefaults.standard.removeObject(forKey: "notifType10")
        statusLabel.layer.cornerRadius = 7
        
        self.clientPicture.layer.cornerRadius = self.clientPicture.frame.height / 2
        self.clientPicture.clipsToBounds = true
        
        
        self.profilePicture.layer.cornerRadius = self.profilePicture.frame.height / 2
        self.profilePicture.clipsToBounds = true
        
        self.directionChanged.layer.cornerRadius = self.directionChanged.frame.height / 2
        self.directionChanged.clipsToBounds = true
        
        if hasLocationPermission(){
            self.locationManager.startUpdatingLocation()
        }
        
        mapView.isMyLocationEnabled = false
        mapView.settings.myLocationButton = false
        mapView.settings.compassButton = false
        
        self.profilePicture.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.openFloatingButton)))
        
        tapDest = UITapGestureRecognizer(target: self, action: #selector(self.zoomAddress(_:)))
        tapAddress = UITapGestureRecognizer(target: self, action: #selector(self.zoomAddress(_:)))
        
        self.addressLabel.addGestureRecognizer(tapAddress)
        self.destinationLabel.addGestureRecognizer(tapDest)
        
        directionChanged.contentHorizontalAlignment = .fill
        directionChanged.contentVerticalAlignment = .fill
        directionChanged.imageView?.contentMode = .scaleToFill
        
        self.setupDropDown()
        
        fetchPrices()
        //UserDefaults.standard.removeObject(forKey: "notifType10")
        
        //check for current driver courses
        Driver.fetchCurrentDriver { (snap) in
            self.driver = Driver(snap: snap!)
            
            if let pic = self.driver.pictureURL{
                
                self.clientPicture.image = UIImage(named: "profileRegestration")
                
                if pic.isValidURL(){
                    self.profilePicture.sd_setImage(with: URL(string: pic), completed: { (image, error, cache, url) in
                        if error == nil {
                            self.profilePicture.image = image
                        }
                    })
                }else if pic.count > 0 {
                    let storageRef = Storage.storage().reference()
                    let profileImagesRef = storageRef.child("ProfilePictures/\(pic)")
                    
                    // You can also access to download URL after upload.
                    profileImagesRef.downloadURL { (url, error) in
                        guard let downloadURL = url else {
                            // Uh-oh, an error occurred!
                            return
                        }
                        self.profilePicture.sd_setImage(with: downloadURL, completed: { (image, error, cache, url) in
                            if error == nil {
                                self.profilePicture.image = image
                            }
                        })
                        
                    }
                }
            }
            if self.driver.status == 2{
                
                Toast(text: "You have an ongoing trip status = 2", duration: Delay.long).show()
                self.showSpinner(onView: self.view)
                
                if let json = UserDefaults.standard.value(forKey: "notifType10") as? String {
                    
                    self.jsonCourseData = JSON.init(parseJSON: json)
                    
                    Course.fetchCurrentTripDetails(courseId: self.jsonCourseData["tripId"].stringValue, completionBlock: { (snap) in
                        self.currentCourse = Course(snap: snap!)
                        self.loadInProgressCourse()
                    })
                }else {
                    self.removeSpinner()
                    self.connect(connected: false)
                    Toast(text: "Error occured while parsing notifType10", duration: Delay.long).show()
                }
                
            }else if self.driver.status == 1 {
                self.connect(connected: false)
                UserDefaults.standard.removeObject(forKey: "notifType10")
                
            }else if self.driver.status == 0 {
                self.connect(connected: false)
                UserDefaults.standard.removeObject(forKey: "notifType10")
            }
        }
        
        //let presenceRef = Database.database().reference(withPath: "Users");
        // Write a string when this client loses connection
        //presenceRef.child((Auth.auth().currentUser?.uid)!).child("status").onDisconnectSetValue("0")
    }
    
    
    func setupDropDown() {
        // The view to which the drop down will appear on
        dropDown.anchorView = self.cancelButton // UIView or UIBarButtonItem
        dropDown.direction = .bottom
        dropDown.cellHeight = 60
        dropDown.width = 300
        
        // The list of items to display. Can be changed dynamically
        dropDown.dataSource = subjects
        
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            if self.currentCourse != nil {
                if index != 2 {
                    self.currentCourse.tripCanceledByDriver(reason: item)
                    UserDefaults.standard.removeObject(forKey: "notifType10")
                    
                    Course.removeCourseObserver(courseId: self.currentCourse.uid, handle: self.refHandle)
                    
                    let banner = NotificationBanner(title: "Trip Canceled", subtitle: "You have canceled the trip", style: .warning)
                    banner.show()
                    
                    if let timer = self.smsTimer {
                        timer.invalidate()
                        self.smsTimer = nil
                        self.smsTimerValue = 0
                    }
                    
                    self.showInitialMainViews(completion: { (done) in})
                    self.clearMap()
                }
            }
            self.dropDown.hide()
        }
        dropDown.cellConfiguration = { [unowned self] (index, item) in
            return self.subjects[index]
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.transparentNavigationBar()
        
        if let type = UserDefaults.standard.string(forKey: "mapType"){
            switch type{
            case "hybrid" :
                mapView.mapType = .hybrid
            case "terrain" :
                mapView.mapType = .terrain
            case "satellite" :
                mapView.mapType = .satellite
            default:
                mapView.mapType = .normal
            }
        }
        
    }
    
    
    @IBAction func cancelTrip(_ sender: UIButton) {
        self.dropDown.show()
    }
    
    @IBAction func openGoogleDirection(_ sender: Any) {
        let destination = Position(lat: 0, lng: 0, address: "")
        if self.currentCourse.status == "accepted" {
            destination.latitude = self.getNotification(from: self.jsonCourseData).pickUpLatitude
            destination.longitude = self.getNotification(from: self.jsonCourseData).pickUpLongitude
            
        }else if self.currentCourse.status == "arrived" || self.currentCourse.status == "started"{
            destination.latitude = self.getNotification(from: self.jsonCourseData).destinationLatitude
            destination.longitude = self.getNotification(from: self.jsonCourseData).destinationLongitude
        }
        
        if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
            UIApplication.shared.open((URL(string:
                "comgooglemaps://?saddr=&daddr=\(destination.latitude!),\(destination.longitude!)&directionsmode=driving")!), options: [:], completionHandler: nil)
            
        } else {
            NSLog("Can't use comgooglemaps://");
        }
    }
    
    
    @IBAction func directionChanged(_ sender: Any) {
        if self.currentCourse != nil {
            self.directionChanged.isSelected = true
            self.currentCourse.courseHasChanged = 1
            self.currentCourse.tripHasChanged()
        }
    }
    
    @IBAction func callClient(_ sender: UIButton) {
        let number = self.getNotification(from: jsonCourseData).phoneNumber
        guard let numberTel = URL(string: "tel://" + number) else { return }
        UIApplication.shared.open(numberTel)
    }
    
    @IBAction func arrivedStartedEndTripAction(_ sender: UIButton) {
        
        if startTrip.title(for: .normal) == "Arrived" {
            
            self.arrivedToLocation()
            
        }else if startTrip.title(for: .normal) == "Start Trip"{
            
            self.startTripFunc()
            
        }else if startTrip.title(for: .normal) == "End Trip"{
            
            self.endTrip()
        }
        
    }
    @IBAction func startWaitingTime(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        
        if !sender.isSelected{
            let waitingStruct = WaitingTimeStruct(duration: waitingTimerValue, status: self.currentCourse.status)
            waitingTimer.invalidate()
            self.currentCourse.setWaitingTime(waitingTime: waitingStruct)
            waitingTimerValue = 0
            timerLabel.isHidden = true
            self.timerLabel.text = ""
            
        }else {
            timerLabel.isHidden = false
            waitingTimer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(self.updateTimerLaber)), userInfo: nil, repeats: true)
            self.showAlertView(isOneButton: true, title: "Click to STOP waiting time", cancelButtonTitle: "STOP TIMER", submitButtonTitle: "", cancelHandler: {
                sender.isSelected = !sender.isSelected
                let waitingStruct = WaitingTimeStruct(duration: self.waitingTimerValue, status: self.currentCourse.status)
                self.waitingTimer.invalidate()
                self.currentCourse.setWaitingTime(waitingTime: waitingStruct)
                self.waitingTimerValue = 0
                self.timerLabel.isHidden = true
                self.timerLabel.text = ""
            }, doneHandler: nil)
        }
    }
    
    @objc func zoomAddress(_ sender : UITapGestureRecognizer){
        let addressVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: AddressDetailViewController.storyboardId) as! AddressDetailViewController
        addressVC.addressStr = addressLabel.text ?? "Unknown"
        if sender == tapDest{
            addressVC.addressStr = destinationLabel.text ?? "Unknown"
        }
        let popupVC = STPopupController(rootViewController: addressVC)
        popupVC.style = .formSheet
        popupVC.present(in: self)
        
    }
    
    @objc func updateTimerLaber(){
        DispatchQueue.main.async {
            self.timerLabel.text = String(format:"%02i min", self.waitingTimerValue / 60) //This will update the label.
        }
        waitingTimerValue += 1
    }
    
    
    
    @objc func updateSMSTimer(){
        // 120 sec : 2 min
        if smsTimerValue >= 120 {
            if smsTimerValue == 120{
                if let timer = self.smsTimer {
                    timer.invalidate()
                    self.smsTimer = nil
                    self.smsTimerValue = 0
                }
                self.sendSMS(idClient: self.currentCourse.clientId!)
                
            }else {
                smsTimerValue -= 1
            }
        }else {
            if let timer = self.smsTimer {
                timer.invalidate()
                self.smsTimer = nil
                self.smsTimerValue = 0
            }
            self.sendSMS(idClient: self.currentCourse.clientId!)
            
        }
        
        
    }
    
    func sendSMS(idClient : String) {
        let body = "Driver will arrive to pickup point in 2 minutes"
        Alamofire.request(CURRENT_DB_REF_APP + "sendsms", method: .get, parameters: ["idclient": idClient,"smsBody" : body], encoding: URLEncoding.default, headers: nil).responseString { (response) in
            switch response.result{
            case .success(let value):
                print(value)
                Toast(text: value, duration: Delay.long).show()
                
                if let timer = self.smsTimer {
                    timer.invalidate()
                    self.smsTimer = nil
                    self.smsTimerValue = 0
                }
                
            case .failure(_):
                print(response.description)
                if let timer = self.smsTimer {
                    timer.invalidate()
                    self.smsTimer = nil
                    self.smsTimerValue = 0
                }
                
                break;
            }
        }
    }
    
    @objc func clientDidCancelRace(){
        
        let banner = NotificationBanner(title: "Trip Canceled", subtitle: "Customer has cancel the trip", style: .warning)
        banner.show()
    }
    
    
    func fetchPrices(){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        let fetchCountReq = NSFetchRequest<NSFetchRequestResult>(entityName: "Prices")
        do {
            let test = try context.fetch(fetchCountReq)
            self.prices = [Price]()
            for object in test as! [NSManagedObject]{
                let price = Price(category: object.value(forKey:"category") as? String,
                                  pricePerKm: (object.value(forKey:"pricePerKm") as! Double),
                                  pricePerMin: (object.value(forKey:"pricePerMin") as! Double),
                                  waintingCost: (object.value(forKey:"waitingCost") as! Double),
                                  baseFare: (object.value(forKey:"baseFare") as! Double),
                                  priceTenMin: (object.value(forKey:"priceTenMin") as! Double))
                self.prices.append(price)
            }
        } catch  {
            print(error)
        }
    }
    // Menu functions
    func collapseHeader() {
        
        UIView.animate(withDuration: 0.5, animations: {
            
            self.floatingButtonsHeight.constant = self.minHeaderHeight
            
            self.floatingButtons.isHidden = true
            
            self.view.layoutIfNeeded()
        })
    }
    
    func expandHeader() {
        
        UIView.animate(withDuration: 0.5, animations: {
            
            self.floatingButtons.isHidden = false
            
            self.floatingButtonsHeight.constant = self.maxHeaderHeight
            
            self.view.layoutIfNeeded()
        })
    }
    
    @objc func openFloatingButton() {
        
        if floatingButtonsOpened {
            collapseHeader()
        }else{
            expandHeader()
        }
        floatingButtonsOpened = !floatingButtonsOpened
    }
    
    
    func connect(connected : Bool)  {
        if connected {
            locationManager.startUpdatingLocation()
            statusLabel.backgroundColor = UIColor.green
            onOff.isHidden = true
            startStopButton.isSelected = true
            Driver.connectDriver(status: 1)
        }else {
            locationManager.stopUpdatingLocation()
            statusLabel.backgroundColor = UIColor.red
            onOff.isHidden = false
            startStopButton.isSelected = false
            Driver.connectDriver(status: 0)
        }
    }
    
    @IBAction func startRace(_ sender: Any) {
        self.connect(connected: true)
    }
    
    
    func clearPath(){
        if polyline != nil {
            polyline.path = nil
            polyline.map = nil
        }
    }
    
    func startTripFunc() {
        print("start started")
        
        Course.tripStarted(tripId: self.jsonCourseData["tripId"].stringValue)
        
        self.startTrip.setTitle("End Trip", for: .normal)
        self.startTrip.backgroundColor = UIColor(hex: 0xcc1243)
    }
    
    
    func endTrip() {
        print("trip finished")
        
        if waitingTimerValue > 0 {
            
            self.showAlertView(isOneButton: true, title: "STOP wainting time first", cancelButtonTitle: "Cancel", submitButtonTitle: "Cancel", cancelHandler: nil, doneHandler: nil)
            
        }else {
            
            self.currentCourse.finishedAt = Date().getFormattedDate("yyyy-MM-dd'T'HH:mm:ssZ")
            
            let tripPrice = self.prices.first(where: { (price) -> Bool in
                price.category == self.currentCourse.classCar
            })
            
            if self.currentCourse.courseHasChanged == 1 {
                //calculate real price depend on finish and start time
                
                if let wt = self.currentCourse.waitingTime{
                    var freeWTBeforeArrive = 0
                    var freeWTAfterArrive = 0
                    var costWT : Double = 0
                    
                    for i in wt{
                        if i.status == "started"{
                            freeWTAfterArrive += i.duration
                        }else {
                            freeWTBeforeArrive += i.duration
                        }
                        if i.duration > 300{//5 mn in seconds
                            costWT += Double(Double(i.duration - 300) / 60).rounded(.up) // number of paied minutes
                        }
                    }
                    
                    let tripDurationInSec = self.currentCourse.finishedAt!.timeIntervalSince(self.currentCourse.startedAt!)
                    
                    let tripActualDuration = Int(tripDurationInSec) -  freeWTAfterArrive < 0 ? 0 : Int(tripDurationInSec) -  freeWTAfterArrive
                    
                    let pricev = ((Double(tripActualDuration) / 60) / 10).rounded(.up) //10 = price per 10 mn \ number of [10]
                    
                    let value = tripPrice!.baseFare + ((Double(pricev) - 1) * tripPrice!.priceTenMin) // -1 : minus first ten minutes
                    
                    let finalCost =  value + (costWT * tripPrice!.waintingCost)
                    
                    //self.currentCourse.finalCost = finalCost
                    
                    self.currentCourse.tripFinished(finalCostAux: finalCost)
                    
                }else{
                    
                    let tripDurationInSec = self.currentCourse.finishedAt!.timeIntervalSince(self.currentCourse.startedAt!)
                    let pricev = ((Double(tripDurationInSec) / 60) / 10).rounded(.up) //price per 10 mn
                    
                    let value = tripPrice!.baseFare + ((Double(pricev) - 1) * tripPrice!.priceTenMin)
                    
                    let finalCost =  value
                    
                    //self.currentCourse.finalCost = finalCost
                    
                    self.currentCourse.tripFinished(finalCostAux: finalCost)
                }
                
            }else if self.currentCourse.courseHasChanged == 0 {
                
                if let wt = self.currentCourse.waitingTime{
                    
                    var costWT : Double = 0
                    
                    for i in wt{
                        if i.duration > 300{//5 mn in seconds
                            costWT += Double(Double(i.duration - 300) / 60).rounded(.up)
                        }
                    }
                    
                    let finalCost =  self.currentCourse.estimatedCost! + (costWT * tripPrice!.waintingCost)
                    
                    //self.currentCourse.finalCost = finalCost
                    
                    self.currentCourse.tripFinished( finalCostAux: finalCost)
                }else{
                    let finalCost =  self.currentCourse.estimatedCost!
                    
                    //self.currentCourse.finalCost = finalCost
                    
                    self.currentCourse.tripFinished( finalCostAux: finalCost)
                }
            }
        }
        
    }
    
    
    func arrivedToLocation() {
        print("arrived To location")
        
        self.waitingButton.isEnabled = true
        
        self.startTrip.setTitle("Start Trip", for: .normal)
        self.startTrip.backgroundColor = UIColor(hex: 0x0e8dcf)
        
        if clientPosition != nil {
            if clientPosition.marker != nil {
                clientPosition.marker.map = nil
                clientPosition.marker = nil
            }
        }
        
        if let timer = self.smsTimer {
            timer.invalidate()
            self.smsTimer = nil
            self.smsTimerValue = 0
        }
        
        Course.arrivedToPickUpPoint(tripId: self.jsonCourseData["tripId"].stringValue)
        
        let destinationPosition = Position(lat: self.getNotification(from: self.jsonCourseData).destinationLatitude,
                                           lng: self.getNotification(from: jsonCourseData).destinationLongitude,
                                           address:self.getNotification(from: jsonCourseData).destinationAddress )
        destinationPosition.marker.icon = #imageLiteral(resourceName: "pos1-1")
        destinationPosition.marker.map = self.mapView
        self.clearPath()
        let coordinate = CLLocationCoordinate2D(latitude: self.clientPosition.latitude!, longitude: self.clientPosition.longitude!)
        
        self.getPolylineRoute(from: coordinate, to: destinationPosition.marker.position)
    }
    
    
    @objc func didAcceptTrip(_ notification: NSNotification) {
        
        if let object = notification.object as? JSON{
            
            self.jsonCourseData = object
            
            UserDefaults.standard.set(self.jsonCourseData.rawString(), forKey: "notifType10")
            
            Driver.connectDriver(status: 2)
            
            Toast(text: "You have accept a trip", duration: Delay.long).show()
            
            self.showSpinner(onView: self.view)
            
            self.refHandle = Course.getTripDetails(courseId: getNotification(from: jsonCourseData).tripId, completionBlock: { (snap) in
                self.removeSpinner()
                
                self.currentCourse = Course(snap: snap!)
                
                print("Course.getTripDetails didAcceptTrip")
                
                // if self.startTrip.title(for: .normal) != "End Trip"{
                
                if self.currentCourse.status == "accepted"{
                    //check waiting time error
                    if let _ = self.currentCourse.waitingTime{
                        self.currentCourse.deleteWT()
                    }
                }

                if self.currentCourse.status == "canceled"{
                    
                    UserDefaults.standard.removeObject(forKey: "notifType10")
                    
                    Course.removeCourseObserver(courseId: self.currentCourse.uid, handle: self.refHandle)
                    
                    let banner = NotificationBanner(title: "Trip Canceled", subtitle: "Customer has cancel the trip", style: .warning)
                    banner.show()
                    
                    self.clearMap()

                    self.showInitialMainViews(completion: { (done) in})
                    //}
                }else if self.currentCourse.status == "finished"{
                    
                    UserDefaults.standard.removeObject(forKey: "notifType10")
                    
                    Course.removeCourseObserver(courseId: self.currentCourse.uid, handle: self.refHandle)
                    
                    self.showInitialMainViews() { (done) in
                        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: FactureTableViewController.storyboardId) as! FactureTableViewController
                        vc.currentCourse = self.currentCourse
                        vc.tripFinishedDelegate = self
                        vc.jsonCourseData = self.jsonCourseData
                        let popupVC = STPopupController(rootViewController: vc)
                        popupVC.style = .formSheet
                        popupVC.present(in: self)
                    }
                }
            })
            
            self.startTrip.setTitle("Arrived", for: .normal)
            self.startTrip.backgroundColor = UIColor(hex: 0x11db6c)
            
            self.loadMainView(json: object)
            
            let driverCoordinate = CLLocationCoordinate2D(latitude: self.currentPosition.latitude!, longitude: self.currentPosition.longitude!)
            
            self.getPolylineRoute(from: driverCoordinate, to: self.clientPosition.marker.position)
            self.getRoadDetails(from: driverCoordinate, to: self.clientPosition.marker.position)
        }
    }
    
    
    //reopen in progress trip
    func loadInProgressCourse() {
        
        self.loadMainView(json: self.jsonCourseData)
        
        self.directionChanged.isSelected = self.currentCourse.courseHasChanged == 1
        
        
        if self.currentCourse.status == "started" || self.currentCourse.status == "arrived"{
            
            let destinationPosition = Position(lat: self.currentCourse.destination.latitude!,
                                               lng: self.currentCourse.destination.longitude! ,
                                               address : self.currentCourse.destination.address! )
            destinationPosition.marker.icon = #imageLiteral(resourceName: "pos1-1")
            destinationPosition.marker.map = self.mapView
            
            if self.currentPosition != nil {
                let coordinate = CLLocationCoordinate2D(latitude: self.currentPosition.latitude!, longitude: self.currentPosition.longitude!)
                self.getPolylineRoute(from: coordinate, to: destinationPosition.marker.position)
            }else{
                let coordinate = CLLocationCoordinate2D(latitude: self.currentCourse.pickUpPoint!.latitude!, longitude: self.currentCourse.pickUpPoint!.longitude!)
                self.getPolylineRoute(from: coordinate, to: destinationPosition.marker.position)
            }
            
            
            if self.clientPosition != nil {
                if self.clientPosition.marker != nil {
                    self.clientPosition.marker.map = nil
                    self.clientPosition.marker = nil
                }
            }
            
            if let timer = self.smsTimer {
                timer.invalidate()
                self.smsTimer = nil
                self.smsTimerValue = 0
            }
            
            self.waitingButton.isEnabled = true
            
            
            if self.currentCourse.status == "started" {
                self.startTrip.setTitle("End Trip", for: .normal)
                self.startTrip.backgroundColor = UIColor(hex: 0xcc1243)
            }else{
                self.startTrip.setTitle("Start Trip", for: .normal)
                self.startTrip.backgroundColor = UIColor(hex: 0x0e8dcf)
            }
        }
        
        if self.currentCourse.status == "accepted"{
            
            //check waiting time error
            if let _ = currentCourse.waitingTime{
                self.currentCourse.deleteWT()
            }
            
            self.waitingButton.isEnabled = false
            
            self.startTrip.setTitle("Arrived", for: .normal)
            self.startTrip.backgroundColor = UIColor(hex: 0x11db6c)
            
            let coordinate : CLLocationCoordinate2D!
            if self.currentPosition != nil {
                coordinate = CLLocationCoordinate2D(latitude: self.currentPosition.latitude!, longitude: self.currentPosition.longitude!)
            }else {
                coordinate = CLLocationCoordinate2D(latitude: self.currentCourse.pickUpPoint!.latitude!, longitude: self.currentCourse.pickUpPoint!.longitude!)
            }
            
            self.clientPosition = Position(lat: self.currentCourse.pickUpPoint.latitude!,
                                           lng: self.currentCourse.pickUpPoint.longitude! ,
                                           address : self.currentCourse.pickUpPoint.address! )
            self.clientPosition.marker.icon = #imageLiteral(resourceName: "PICKUP")
            self.clientPosition.marker.map = self.mapView
            
            self.getPolylineRoute(from: coordinate, to: self.clientPosition.marker.position)
            
            self.getRoadDetails(from: coordinate, to: self.clientPosition.marker.position)
        }
        
        if self.currentCourse.status == "finished" || self.currentCourse.status == "canceled"{
            self.connect(connected: true)
            self.directionChanged.isSelected = false
            UserDefaults.standard.removeObject(forKey: "notifType10")
            self.waitingButton.isEnabled = false
            
            if let _ = self.smsTimer {
                self.smsTimer.invalidate()
                self.smsTimer = nil
                self.smsTimerValue = 0
            }
            
            Toast(text: "You have a finished/canceled trip", duration: Delay.long).show()
        }
        
        
        self.refHandle = Course.getTripDetails(courseId: getNotification(from: jsonCourseData).tripId, completionBlock: { (snap) in
            self.removeSpinner()
            self.currentCourse = Course(snap: snap!)
            
            print("Course.getTripDetails loadInProgressCourse")
            
            //if self.startTrip.title(for: .normal) != "End Trip"{
            
            if self.currentCourse.status == "canceled"{
                
                UserDefaults.standard.removeObject(forKey: "notifType10")
                
                Course.removeCourseObserver(courseId: self.currentCourse.uid, handle: self.refHandle)
                
                let banner = NotificationBanner(title: "Trip Canceled", subtitle: "Customer has cancel the trip", style: .warning)
                banner.show()
                
                if let timer = self.smsTimer {
                    timer.invalidate()
                    self.smsTimer = nil
                    self.smsTimerValue = 0
                }
                
                self.showInitialMainViews(completion: { (done) in})
                self.clearMap()
                //}
            }else if self.currentCourse.status == "finished"{
                UserDefaults.standard.removeObject(forKey: "notifType10")
                
                Course.removeCourseObserver(courseId: self.currentCourse.uid, handle: self.refHandle)
                
                self.showInitialMainViews() { (done) in
                    let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: FactureTableViewController.storyboardId) as! FactureTableViewController
                    vc.currentCourse = self.currentCourse
                    vc.tripFinishedDelegate = self
                    vc.jsonCourseData = self.jsonCourseData
                    let popupVC = STPopupController(rootViewController: vc)
                    popupVC.style = .formSheet
                    popupVC.present(in: self)
                }
            }
        })
        
    }
    
    func showTripInProgressViews()  {
        UIView.animate(withDuration: 0.5, animations: {
            
            self.cancelButton.isHidden = false
            self.directionChanged.isHidden = false
            self.googleMapBtn.isHidden = false
            self.profilePicture.isHidden = true
            self.statusLabel.isHidden = true
            self.floatingButtons.isHidden = true
            self.clientDetailsView.isHidden = false
            
            self.onOff.isHidden = true
            self.startStopButton.isSelected = true
            
            self.bottomDriverViewConstraint.constant = 0
            self.topClientViewConstraint.constant = 0
            
            self.view.layoutIfNeeded()
        })
    }
    
    func showInitialMainViews(completion : @escaping ((Bool) -> Void)){
        UIView.animate(withDuration: 0.5, animations: {
            self.waitingButton.isEnabled = false
            self.waitingButton.isSelected = false
            
            self.directionChanged.isSelected = false
            self.directionChanged.isHidden = true
            self.cancelButton.isHidden = true
            
            self.googleMapBtn.isHidden = true
            self.clientDetailsView.isHidden = true
            
            self.profilePicture.isHidden = false
            self.statusLabel.isHidden = false
            self.floatingButtons.isHidden = false
            
            self.bottomDriverViewConstraint.constant = -200
            self.topClientViewConstraint.constant = -160
            self.view.layoutIfNeeded()
        },completion: completion)
    }
    
    
    
    func loadMainView(json : JSON){
        
        clientPictureUrl = getNotification(from: jsonCourseData).pictureURL
        
        
        self.clientPicture.image = UIImage(named: "profileRegestration")
        
        if clientPictureUrl.isValidURL(){
            self.clientPicture.sd_setImage(with: URL(string: clientPictureUrl), completed: { (image, error, cache, url) in
                if error == nil {
                    self.clientPicture.image = image
                }
            })
        }else if clientPictureUrl.count > 0 {
            let storageRef = Storage.storage().reference()
            let profileImagesRef = storageRef.child("ProfilePictures/\(clientPictureUrl!)")
            
            // You can also access to download URL after upload.
            profileImagesRef.downloadURL { (url, error) in
                guard let downloadURL = url else {
                    // Uh-oh, an error occurred!
                    return
                }
                self.clientPicture.sd_setImage(with: downloadURL, completed: { (image, error, cache, url) in
                    if error == nil {
                        self.clientPicture.image = image
                    }
                })
                
            }
        }
        
        clientPhoneNumber.text = self.getNotification(from: jsonCourseData).phoneNumber
        clientName.text = self.getNotification(from: jsonCourseData).fullName
        
        addressLabel.text = self.getNotification(from: jsonCourseData).pickUpAddress
        destinationLabel.text = self.getNotification(from: jsonCourseData).destinationAddress
        
        self.clientPosition = Position(lat: self.getNotification(from: jsonCourseData).pickUpLatitude,
                                       lng: self.getNotification(from: jsonCourseData).pickUpLongitude,
                                       address : self.getNotification(from: jsonCourseData).pickUpAddress)
        
        self.clientPosition.marker.icon = #imageLiteral(resourceName: "PICKUP")
        self.clientPosition.marker.map = self.mapView
        
        
        
        self.showTripInProgressViews()
        
    }
    
    func clearMap(){
        self.startTrip.setTitle("Arrived", for: .normal)
        self.startTrip.backgroundColor = UIColor(hex: 0x11db6c)
        self.jsonCourseData = nil
        self.currentCourse = nil
        
        self.waitingTimerValue = 0
        self.smsTimerValue = 0
        
        if waitingTimer != nil {
            waitingTimer.invalidate()
            waitingTimer = nil
        }
        
        if smsTimer != nil {
            smsTimer.invalidate()
            smsTimer = nil
        }
        
        self.connect(connected: true)
        
        self.clearPath()
        //self.currentPosition = nil
        self.mapView.clear()
    }
    
    @IBAction func menuAction(_ sender: UIButton) {
        switch sender.tag {
        case 0:
            connect(connected: !self.startStopButton.isSelected)
            break
            
        case 1: break
            
        case 2 : break
            //self.navigationController?.pushViewController(UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: SettingsTableViewController.storyboardId) as! SettingsTableViewController, animated: true)
            
        default: break
            
        }
    }
    
    @objc func openClientPicture() {
        
    }
}
// MARK: - Extentions
extension MainDriverViewController {
    
    
    // MARK :- Draw roads
    func getPolylineRoute(from source: CLLocationCoordinate2D, to destination: CLLocationCoordinate2D){
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        let url = URL(string: "https://maps.googleapis.com/maps/api/directions/json?origin=\(source.latitude),\(source.longitude)&destination=\(destination.latitude),\(destination.longitude)&sensor=true&mode=driving&key=\(directionsApiKey)")!
        //let url = URL(string: "https://maps.googleapis.com/maps/api/directions/json?origin=Disneyland&destination=Universal+Studios+Hollywood&key=AIzaSyCm2kW7TPsr8F4gWWKy7blOIteUkvDOmHU")
        
        let task = session.dataTask(with: url, completionHandler: { (data, response, error) in
            if error != nil {
                print(error!.localizedDescription)
                //self.activityIndicator.stopAnimating()
            }
            else {
                do {
                    if let json : [String:Any] = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any]{
                        
                        guard let routes = json["routes"] as? NSArray else {
                            
                            return
                        }
                        
                        if (routes.count > 0) {
                            let overview_polyline = routes[0] as? NSDictionary
                            let dictPolyline = overview_polyline?["overview_polyline"] as? NSDictionary
                            
                            let points = dictPolyline?.object(forKey: "points") as? String
                            DispatchQueue.main.async {
                                //self.activityIndicator.stopAnimating()
                                self.showPath(polyStr: points!)
                                let bounds = GMSCoordinateBounds(coordinate: source, coordinate: destination)
                                let update = GMSCameraUpdate.fit(bounds, with: UIEdgeInsetsMake(170, 30, 30, 30))
                                self.mapView!.moveCamera(update)
                            }
                        }
                        else {
                            print("routes = 0")
                        }
                    }
                }
                catch {
                    print("error in JSONSerialization")
                    
                }
            }
        })
        task.resume()
    }
    
    
    
    func getRoadDetails(from source: CLLocationCoordinate2D, to destination: CLLocationCoordinate2D)  {
        
        //  url = https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&departure_time=\(String(describing: Date().addSeconds(secondsToAdd: 300).timeIntervalSince1970))&traffic_model=
        Alamofire.request("https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&departure_time=now&origins=\(source.latitude),\(source.longitude)&destinations=\(destination.latitude),\(destination.longitude)&key=\(matrixApiKey)", method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            switch response.result {
            case .failure(_):
                print(response.description)
                break;
            case .success(let value):
                let json = JSON(value)
                
                if  json.isEmpty || json["status"].stringValue != "OK"{
                    print("no data available")
                    break
                }
                let rows = json["rows"].arrayValue
                let elements = rows.first!["elements"].arrayValue
                let roadDetails = elements.first!
                if (roadDetails["duration_in_traffic"] != JSON.null) {
                    let traffic = roadDetails["duration_in_traffic"]
                    let trafficDurationValue = traffic["value"].intValue
                    
                    //self.smsTimerValue = Int(Float(trafficDurationValue / 60).rounded(.toNearestOrAwayFromZero))
                    self.smsTimerValue = trafficDurationValue
                    if self.smsTimer == nil {
                        self.smsTimer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(self.updateSMSTimer)), userInfo: nil, repeats: true)
                    }
                }
            }
        }
    }
    
    func showPath(polyStr :String){
        let path = GMSPath(fromEncodedPath: polyStr)
        polyline = GMSPolyline(path: path)
        polyline.strokeWidth = 3.0
        polyline.strokeColor = UIColor(hex: 0x0e8dcf)
        polyline.map = mapView // Your map view
    }
    
    func getNotification(from userInfo: JSON) -> NotificationStruct {
        print("userInfo: ", userInfo)
        
        var type = 0
        
        var pickUpAddress = ""
        
        var pickUpLatitude : Double = 0
        
        var pickUpLongitude : Double = 0
        
        var destinationAddress = ""
        
        var destinationLatitude : Double = 0
        
        var destinationLongitude : Double = 0
        
        var initialDistance : Int = 0
        
        var estimatedCost : Int = 0
        
        var estimatedTime : Int = 0
        
        var fullName = ""
        
        var sentAt = ""
        
        var phoneNumber = ""
        
        var pictureURL = ""
        
        var tripId = ""
        
        
        if let t = userInfo["type"].int {
            type = t
        }
        
        if  let objectId =  userInfo["pickUpAddress"].string{
            pickUpAddress = objectId
        }
        
        if  let objectId =  userInfo["pickUpLatitude"].double{
            pickUpLatitude = objectId
        }
        
        if  let objectId =  userInfo["pickUpLongitude"].double{
            pickUpLongitude = objectId
        }
        
        if  let objectId =  userInfo["destinationAddress"].string{
            destinationAddress = objectId
        }
        if  let objectId =  userInfo["destinationLatitude"].double{
            destinationLatitude = objectId
        }
        if  let objectId =  userInfo["destinationLongitude"].double{
            destinationLongitude = objectId
        }
        if  let objectId =  userInfo["initialDistance"].int{
            initialDistance = objectId
        }
        if  let objectId =  userInfo["estimatedCost"].int{
            estimatedCost = objectId
        }
        if  let objectId =  userInfo["estimatedTime"].int{
            estimatedTime = objectId
        }
        if  let firstname =  userInfo["firstName"].string , let lastname =  userInfo["lastName"].string{
            fullName = firstname + " " + lastname
        }
        if  let objectId =  userInfo["phoneNumber"].string{
            phoneNumber = objectId
        }
        if  let objectId =  userInfo["pictureURL"].string{
            pictureURL = objectId
        }
        
        if  let objectId =  userInfo["tripId"].string{
            tripId = objectId
        }
        
        if  let objectId =  userInfo["sentAt"].string{
            sentAt = objectId
        }
        
        let notification = NotificationStruct.init(type: type, pickUpAddress: pickUpAddress, pickUpLatitude: pickUpLatitude, pickUpLongitude: pickUpLongitude, destinationAddress: destinationAddress, destinationLatitude: destinationLatitude, destinationLongitude: destinationLongitude, initialDistance: initialDistance, estimatedTime: estimatedTime, estimatedCost: estimatedCost, fullName: fullName, pictureURL: pictureURL, phoneNumber: phoneNumber, tripId: tripId, sentAt: sentAt)
        
        
        return notification
    }
    
}
// MARK: - CLLocationManagerDelegate

extension MainDriverViewController : CLLocationManagerDelegate{
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        guard status == .authorizedWhenInUse ||  status == .authorizedAlways else {
            
            let alertController = UIAlertController(title: "Location Permission Required", message: "Please enable location permissions in settings.", preferredStyle: UIAlertControllerStyle.alert)
            
            let okAction = UIAlertAction(title: "Settings", style: .default, handler: {(cAlertAction) in
                //Redirect to Settings app
                UIApplication.shared.open(URL(string:UIApplicationOpenSettingsURLString)!)
            })
            
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel)
            alertController.addAction(cancelAction)
            
            alertController.addAction(okAction)
            
            self.present(alertController, animated: true, completion: nil)
            
            return
        }
        
        locationManager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else {
            return
        }
        Driver.updateLocation(position: location)
        
        if currentPosition == nil {
            self.currentPosition = Position(lat: location.coordinate.latitude, lng: location.coordinate.longitude , address : "")
            self.currentPosition.marker!.rotation = location.course
            self.currentPosition.marker!.icon = #imageLiteral(resourceName: "navigation")
            self.currentPosition.marker!.map = self.mapView
        }else {
            self.currentPosition.marker!.icon = #imageLiteral(resourceName: "navigation")
            self.currentPosition.marker!.rotation = location.course
            self.currentPosition.marker.position = location.coordinate
            self.currentPosition.longitude = location.coordinate.longitude
            self.currentPosition.latitude = location.coordinate.latitude
        }
        mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 18, bearing:location.course , viewingAngle: 0)
    }
    
    func hasLocationPermission() -> Bool {
        var hasPermission = false
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                hasPermission = false
            case .authorizedAlways, .authorizedWhenInUse:
                hasPermission = true
            }
        } else {
            hasPermission = false
        }
        
        return hasPermission
    }
}
extension MainDriverViewController : TripDidFinish {
    func tripDidFinish() {
        self.clearMap()
    }
}

