//
//  NotificationViewController.swift
//  MYDRIVER Driver
//
//  Created by sami hazel on 4/23/19.
//  Copyright © 2019 MyDriverKw. All rights reserved.
//

import UIKit
import SwiftyJSON
import AVFoundation
class NotificationViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var notifArray = [JSON]()
    var player: AVAudioPlayer?

    
    var currentDriver : Driver!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        playSound()
        self.currentDriver = Driver.fetchCurrentDriverFromLocal()
    }
    
    func playSound() {
        guard let url = Bundle.main.url(forResource: "CTU24", withExtension: "mp3") else { return }
        
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback, mode: AVAudioSessionModeDefault)
            try AVAudioSession.sharedInstance().setActive(true)
            
            /* The following line is required for the player to work on iOS 11. Change the file type accordingly*/
            player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.mp3.rawValue)
            
            /* iOS 10 and earlier require the following line:
             player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileTypeMPEGLayer3) */
            
            guard let player = player else { return }
            
            player.prepareToPlay()
            player.setVolume(1, fadeDuration: 2)
            player.numberOfLoops = -1
            player.play()
            
        } catch let error {
            print(error.localizedDescription)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension NotificationViewController : UITableViewDelegate , UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.tableView.bounds.height / CGFloat(notifArray.count)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "notifCell", for: indexPath) as! NotificationTableViewCell
        cell.currentDriver = self.currentDriver
        cell.delegate = self
        cell.initNotifCell(json: notifArray[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notifArray.count
    }
}

extension NotificationViewController : DidRespondOnRaceCall{
    func customerDidCancelTrip(idTrip: String) {
        player?.stop()
        
        Driver.connectDriver(status: 1)

        self.dismiss(animated: true, completion: nil)
    }
    
    func didAcceptRace(idTrip: String) {

        player?.stop()
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func didRejectRace(idTrip: String) {
        player?.stop()
        
        Driver.connectDriver(status: 1)
        
        self.dismiss(animated: true, completion: nil)
    }
}
