//
//  NotificationTableViewCell.swift
//  MYDRIVER Driver
//
//  Created by sami hazel on 4/22/19.
//  Copyright © 2019 MyDriverKw. All rights reserved.
//

import UIKit
import SwiftyJSON
import SDWebImage
import ButtonProgressBar_iOS
import FirebaseDatabase

protocol DidRespondOnRaceCall {
    func didAcceptRace(idTrip : String)
    func didRejectRace(idTrip : String)
    func customerDidCancelTrip(idTrip : String)
}
class NotificationTableViewCell: UITableViewCell {

    
    @IBOutlet weak var acceptButton: ButtonProgressBar!
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var pickupAdress: UILabel!
    @IBOutlet weak var raceEstimatedTime: UILabel!
    
    var tripId : String!
    var delegate : DidRespondOnRaceCall!
    var data : JSON!
    //var raceJson : JSON!
    let shapeLayer = CAShapeLayer()

    var currentCourse : Course!
    
    var currentDriver : Driver!
    
    //@IBOutlet weak var timerView: UIView!
    
    var timer = Timer()

    var max = 15
    var refHandle : UInt!
    
    func initNotifCell(json : JSON) {
        self.data = json
        self.tripId = json["tripId"].stringValue
        
        self.acceptButton.isEnabled = false
        
        refHandle = Course.getTripDetails(courseId: self.tripId) { (snap) in
            self.currentCourse = Course(snap: snap!)
            self.acceptButton.isEnabled = true
            print("Course.getTripDetails NotificationTableViewCell")
        }
        
        self.max = 15
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        
        let date = Date()
        if let sentAt = dateFormatter.date(from: json["sentAt"].stringValue){
            self.max = 15 - Int(date.timeIntervalSince(sentAt))
        }
        self.timerLabel.text = " \(max)"

        pickupAdress.text = json["pickUpAddress"].stringValue
        
        raceEstimatedTime.text =  String(format:"%02i min", json["estimatedTime"].int! / 60)

        acceptButton.startIndeterminate(withTimePeriod: Double(15), andTimePadding: Double(0.5))
        acceptButton.setProgress(progress: 0, true)
        acceptButton.setProgressColor(color: UIColor(hex: 0x0e8dcf))
        acceptButton.setCompletionImage(image: #imageLiteral(resourceName: "checked"))

        runTimer()
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

    }

    func runTimer() {

        timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(self.updateTimer)), userInfo: nil, repeats: true)

    }
    
    @objc func updateTimer() {
        //This will decrement(count down)the seconds.
        DispatchQueue.main.async {
            self.timerLabel.text = "\(self.max)" //String(format:"%02i:%02i min", self.max / 60, self.max % 60) //This will update the label.
        }
        max -= 1
        if max == 0 {
            acceptButton.stopIndeterminate()
            timer.invalidate()
            Course.removeCourseObserver(courseId: self.currentCourse.uid, handle: self.refHandle)
            delegate.didRejectRace(idTrip: self.tripId)
        }
    }

    
    @IBAction func rejectRace(_ sender: UIButton) {
        acceptButton.stopIndeterminate()
        timer.invalidate()
        Course.removeCourseObserver(courseId: self.currentCourse.uid, handle: self.refHandle)

        delegate.didRejectRace(idTrip: self.tripId)
    }
    
    @IBAction func acceptRace(_ sender: UIButton) {
        acceptButton.triggerCompletion()
        timer.invalidate()
        Course.removeCourseObserver(courseId: self.currentCourse.uid, handle: self.refHandle)

        if self.currentCourse.status == "pending" {
            Driver.acceptRace(courseId: tripId, vehicleId: self.currentDriver.vehicleId)
            NotificationCenter.default.post(name: Notification.Name("did_accept_race"), object: data)
            self.delegate.didAcceptRace(idTrip: tripId)
        }else {
            NotificationCenter.default.post(name: Notification.Name("race_canceled"), object: data)
            self.delegate.customerDidCancelTrip(idTrip: tripId)
        }

    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
