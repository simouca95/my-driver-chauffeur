//
//  SettingsTableViewController.swift
//  MyDriver
//
//  Created by sami hazel on 4/6/19.
//  Copyright © 2019 MyDriverKw. All rights reserved.
//

import UIKit
import FirebaseAuth

class SettingsTableViewController: UITableViewController {
    
    static let storyboardId = "SettingsTableViewController"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func back(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.cancelTransparentNavigationBar()
        
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 4
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
//
//        if indexPath.row == 1 {
//            let message = " "
//            let alertController = UIAlertController.init(title: title, message: message, preferredStyle: .actionSheet)
//
//            let firstChoice = UIAlertAction.init(title: "Arabic", style: .default) { (alert) in
//                if Languages.currentLanguage() != "ar" {
//
//                    Languages.setAppLanguage("ar")
//                    UIView.appearance().semanticContentAttribute = .forceRightToLeft
//                    //UINavigationBar.appearance().semanticContentAttribute = .forceRightToLeft
//
//                    self.flipView()
//                }
//            }
//            alertController.addAction(firstChoice)
//
//            let secondChoice = UIAlertAction.init(title: "English", style: .default){ (alert) in
//                if Languages.currentLanguage() != "en" {
//
//                    Languages.setAppLanguage("en")
//                    UIView.appearance().semanticContentAttribute = .forceRightToLeft
//                    //UINavigationBar.appearance().semanticContentAttribute = .forceRightToLeft
//
//                    self.flipView()
//                }
//            }
//            alertController.addAction(secondChoice)
//
//            let thirdChoice = UIAlertAction.init(title: "Frensh", style: .default){ (alert) in
//                if Languages.currentLanguage() != "fr" {
//
//                    Languages.setAppLanguage("fr")
//                    UIView.appearance().semanticContentAttribute = .forceRightToLeft
//                    //UINavigationBar.appearance().semanticContentAttribute = .forceRightToLeft
//
//                    self.flipView()
//                }
//            }
//            alertController.addAction(thirdChoice)
//
//            let cancelChoice = UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil)
//            alertController.addAction(cancelChoice)
//
//            self.present(alertController, animated: true, completion: nil)
//        }
        if indexPath.row == 2 {
            let alertController = UIAlertController.init(title: "Change the map type", message: "Choose", preferredStyle: .actionSheet)
            
            let firstChoice = UIAlertAction.init(title: "Normal", style: .default) { (alert) in
                UserDefaults.standard.set("normal", forKey: "mapType")
            }
            
            alertController.addAction(firstChoice)
            
            let secondChoice = UIAlertAction.init(title: "Terrain", style: .default) { (alert) in
                UserDefaults.standard.set("terrain", forKey: "mapType")
            }
            alertController.addAction(secondChoice)
            
            let thirdChoice = UIAlertAction.init(title: "Satellite", style: .default) { (alert) in
                UserDefaults.standard.set("satellite", forKey: "mapType")
            }
            alertController.addAction(thirdChoice)
            
            let forthChoice = UIAlertAction.init(title: "Hybrid", style: .default) { (alert) in
                UserDefaults.standard.set("hybrid", forKey: "mapType")
            }
            alertController.addAction(forthChoice)
            
            let cancelChoice = UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil)
            alertController.addAction(cancelChoice)
            
            self.present(alertController, animated: true, completion: nil)
        }
        
        if indexPath.row == 0 {
            
            self.navigationController?.pushViewController(UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: DriverProfileTableViewController.storyboardId) as! DriverProfileTableViewController, animated: true)
        }
        
        if indexPath.row == 3 {
            let firebaseAuth = Auth.auth()
            
            self.showAlertView(isOneButton: false, title: "Would you like to Logout", cancelButtonTitle: "Cancel", submitButtonTitle: "Proceed", cancelHandler: nil) {
                do {
                    Driver.connectDriver(status: 0)
                    try firebaseAuth.signOut()
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let introMain = storyboard.instantiateViewController(withIdentifier: LoginEmailViewController.storyboardId) as! LoginEmailViewController
                    self.present(introMain, animated: true, completion: nil)
                    
                    
                } catch let signOutError as NSError {
                    print ("Error signing out: %@", signOutError)
                }
            }
            
        }
    }
    
    func flipView(){
        
        Localizer.DoTheExchange()
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.rootViewController?.dismiss(animated: false, completion: nil)
        
        let mainStb = UIStoryboard.init(name: "Main", bundle: nil)
        
        appDelegate.window?.rootViewController = mainStb.instantiateInitialViewController()
        
        UIView.transition(with: appDelegate.window!,
                          duration: 0.5,
                          options: .transitionFlipFromLeft,
                          animations: nil,
                          completion: nil)
        
    }

    
}
