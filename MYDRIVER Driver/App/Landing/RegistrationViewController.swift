//
//  RegistrationViewController.swift
//  MyDriver
//
//  Created by sami hazel on 4/6/19.
//  Copyright © 2019 MyDriverKw. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth
import MobileCoreServices
import FirebaseStorage

class RegistrationViewController: UIViewController {

    static let storyboardId = "RegistrationViewController"
    
    @IBOutlet weak var editPicture: UIImageView!

    @IBOutlet weak var userPicture: UIImageView!
    @IBOutlet weak var userEmail: UITextField!
    @IBOutlet weak var username: UITextField!
    var ref: DatabaseReference!
    var imagePickerController: UIImagePickerController!

    var uploadPhoto = false

    override func viewDidLoad() {
        super.viewDidLoad()
        self.ref = Database.database(url: CURRENT_DB_REF).reference()

        // Do any additional setup after loading the view.
        editPicture.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.editUserPicture)))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func register(_ sender: Any) {
        if !username.text!.isBlank {
            if userEmail.text!.isEmail{
                let driverID = Auth.auth().currentUser!.uid
                
                if uploadPhoto {
                    var data = Data()
                    data = UIImageJPEGRepresentation(userPicture.image!, 0.8)!
                    let metaData = StorageMetadata()
                    metaData.contentType = "image/jpg"
                    let name = "\(Date.timeIntervalSinceReferenceDate)"
                    let storageRef = Storage.storage().reference()
                    let profileImagesRef = storageRef.child("ProfilePictures/\(name)")

                    
                    // Upload the file to the path "ProfilePictures/...."
                    profileImagesRef.putData(data, metadata: metaData) { (metaData, error) in
                        if let error = error {
                            print(error.localizedDescription)
                            return
                        }else{
                            // You can also access to download URL after upload.
                            profileImagesRef.downloadURL { (url, error) in
                                guard let _ = url else {
                                    // Uh-oh, an error occurred!
                                    return
                                }
                                let driver = Driver(phoneNumber: "+21688555555", fullName: "sami", id: driverID , pictureURL : name)
                                self.ref.child("Driver").child(driverID).setValue(driver.toDictionary())
                                self.present(UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "mainNVC") as! UINavigationController, animated: true, completion: nil)
                            }
                        }
                    }
                }else{
                    let driver = Driver(phoneNumber: "+21688555555", fullName: "sami", id: driverID)
                    self.ref.child("Driver").child(driverID).setValue(driver.toDictionary())
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    self.present(storyboard.instantiateViewController(withIdentifier: "mainNVC"), animated: true, completion: nil)
                }
            }else{
                userEmail.shake()
            }
        }else{
            username.shake()
        }
    }
    
    @objc func editUserPicture ( ){
        let alert = UIAlertController(title: nil,
                                      message: nil,
                                      preferredStyle: UIAlertControllerStyle.actionSheet)
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("TakePhoto", comment:"TakePhoto"), style: UIAlertActionStyle.default, handler: {(action) in
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
                
                self.imagePickerController = UIImagePickerController()
                self.imagePickerController.delegate = self
                self.imagePickerController.sourceType = UIImagePickerControllerSourceType.camera;
                self.imagePickerController.allowsEditing = true
                self.imagePickerController.mediaTypes = [kUTTypeImage as String]
                self.imagePickerController.navigationBar.barTintColor = self.navigationController?.navigationBar.barTintColor
                self.imagePickerController.navigationBar.tintColor = self.navigationController?.navigationBar.tintColor
                self.imagePickerController.navigationBar.barStyle = UIBarStyle.black
                self.imagePickerController.navigationBar.isTranslucent = false
                
                self.present(self.imagePickerController, animated: true, completion: nil)
                
            }
            
        }))
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("FromLibrary", comment:"FromLibrary"), style: UIAlertActionStyle.default, handler: {(action) in
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
                
                self.imagePickerController = UIImagePickerController()
                self.imagePickerController.delegate = self
                self.imagePickerController.sourceType = .photoLibrary
                self.imagePickerController.allowsEditing = true
                self.imagePickerController.mediaTypes = [kUTTypeImage as String]
                self.imagePickerController.navigationBar.barTintColor = self.navigationController?.navigationBar.barTintColor
                self.imagePickerController.navigationBar.tintColor = self.navigationController?.navigationBar.tintColor
                self.imagePickerController.navigationBar.barStyle = UIBarStyle.black
                self.imagePickerController.navigationBar.isTranslucent = false
                
                self.present(self.imagePickerController, animated: true, completion: nil)
                
            }
            
        }))
        
        alert.view.tintColor = UIColor.gray
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment:"Cancel"), style: UIAlertActionStyle.cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
        
    }
}

extension RegistrationViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {


    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        
        let mediaType = (info[UIImagePickerControllerMediaType] as! String)
        
        if mediaType == "public.image" {
            
            if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
                
                self.editPicture.image = image
                self.uploadPhoto = true
            }
        }
        picker.dismiss(animated: true, completion: nil)

    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        picker.dismiss(animated: true, completion: nil)
    }
    
}
