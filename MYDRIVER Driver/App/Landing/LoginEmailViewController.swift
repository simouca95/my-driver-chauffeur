//
//  LoginEmailViewController.swift
//  MyDriver
//
//  Created by sami hazel on 5/13/19.
//  Copyright © 2019 MyDriverKw. All rights reserved.
//

import UIKit
import FirebaseAuth

class LoginEmailViewController: UIViewController {

    static let storyboardId = "LoginEmailViewController"

    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var email: UITextField!
    
    @IBOutlet weak var loginButton: LoadingButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func login(_ sender: Any) {
        if email.text!.replacingOccurrences(of: " ", with: "").isEmail || password.text!.count == 0{
            loginButton.loading = true
            Auth.auth().signIn(withEmail: email.text! , password: password.text!) { (authResult, error) in
                self.loginButton.loading = false
                if let error = error {
                    print(error.localizedDescription)
                    self.handleError(error)
                    return
                }
                
                if let newToken = UserDefaults.standard.string(forKey: "RegistrationToken"){
                    
                    Driver.updateDeviceToken(Auth.auth().currentUser!.uid, newToken)
                    
                }else{
                    
                    Driver.updateDeviceToken(Auth.auth().currentUser!.uid, "")

                }

                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                self.present(storyboard.instantiateViewController(withIdentifier: "mainNVC") as! UINavigationController, animated: true, completion: nil)
            }
        }else{
            email.shake()
            password.shake()
        }

    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
