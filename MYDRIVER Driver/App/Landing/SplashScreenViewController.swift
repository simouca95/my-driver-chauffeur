//
//  SplashScreenViewController.swift
//  MyDriver
//
//  Created by sami hazel on 4/6/19.
//  Copyright © 2019 MyDriverKw. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import FirebaseAuth
class SplashScreenViewController: UIViewController {
    
    @IBOutlet weak var loadingView: NVActivityIndicatorView!
    
    var window: UIWindow!
    var timer: Timer!
    var count = 0
    var firstLoad = true
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadingView.color = .white
        self.loadingView.type = .semiCircleSpin
        self.loadingView.startAnimating()
        timer = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(openMainPage), userInfo: nil, repeats: false)
        
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func openMainPage() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)

        if Auth.auth().currentUser == nil  {
            let introMain = storyboard.instantiateViewController(withIdentifier: LoginEmailViewController.storyboardId)
            introMain.modalPresentationStyle = .custom
            introMain.modalTransitionStyle = .crossDissolve
            self.present(introMain, animated: true, completion: nil)
        }else{
            Driver.fetchCurrentDriver { (snap) in
                self.loadingView.stopAnimating()
                _ = Driver(snap: snap!)
                self.present(storyboard.instantiateViewController(withIdentifier: "mainNVC") , animated: true, completion: nil)
            }
        }
        

        //        if self.firstLoad {
        //            self.firstLoad = false
        //            if User.current() != nil {
        //                User.current()?.fetchInBackground()
        //                self.window!.rootViewController = goToHome()
        //                self.window!.makeKeyAndVisible()
        //            }else{
        //                let stb = UIStoryboard(name: "Main", bundle: nil)
        //                let introMain = stb.instantiateViewController(withIdentifier: IntroSliderViewController.storyboardId)
        //                introMain.modalPresentationStyle = .custom
        //                introMain.modalTransitionStyle = .crossDissolve
        //                self.present(introMain, animated: true, completion: nil)
        //            }
        //        }
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
