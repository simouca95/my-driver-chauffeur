//
//  PhoneNumberViewController.swift
//  MyDriver
//
//  Created by sami hazel on 4/6/19.
//  Copyright © 2019 MyDriverKw. All rights reserved.
//

import UIKit
import STPopup
import FirebaseAuth

class PhoneNumberViewController: UIViewController {
    
    static let storyboardId = "PhoneNumberViewController"
    
    @IBOutlet weak var countryLabel: UILabel!
    @IBOutlet weak var countryFlag: UILabel!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var phoneNumber: UITextField!
    
    let phoneNumberStr = "+21688555555"
    
    // This test verification code is specified for the given test phone number in the developer console.
    let testVerificationCode = "111111"
    
    var countryCode: String!
    var countrySelector: STPopupController!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        //init coutry code and flag
        if let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String {
            self.countryFlag.text = countryCode.emojiFlag()
            self.countryLabel.text = "+965"

        }else{
            self.countryFlag.text = "KW".emojiFlag()
            self.countryLabel.text = "+965"
        }
        
        let countryVC = CountrySelectorViewController(nibName: "CountrySelectorViewController", bundle: nil)
        countryVC.delegate = self
        self.countrySelector = STPopupController(rootViewController: countryVC)
        self.countryFlag.isUserInteractionEnabled = true
        self.countryFlag.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.selectCountry(_:))))

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        shadowView.shadowWithCornder(raduis: 3, shadowColor: UIColor.black.cgColor, shadowOffset: .zero, shadowOpacity: 0.5, shadowRadius: 2)
        
    }
    
    @IBAction func toActivation(_ sender: Any) {
        Auth.auth().settings!.isAppVerificationDisabledForTesting = true

        let phoneNum = phoneNumberStr//countryLabel.text! + phoneNumber.text!
        if phoneNum.isPhoneNumber{
            PhoneAuthProvider.provider().verifyPhoneNumber(phoneNum, uiDelegate: nil) { (verificationID, error) in
                if let error = error {
                    print(error.localizedDescription)
                    return
                }
                // Sign in using the verificationID and the code sent to the user
                // ...
                UserDefaults.standard.set(verificationID, forKey: "authVerificationID")
                presentVC(vc: ActivationViewController.storyboardId, self: self)

            }
        }
    }
    
    @objc func selectCountry(_ sender: UITapGestureRecognizer){
        self.countrySelector.present(in: self)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}
extension PhoneNumberViewController: CountrySelectorDelegate{
    
    func didSelectCountry(country: Country, viewController: UIViewController) {
        
        viewController.dismiss(animated: true) {
            
            self.countryFlag.text = country.code.emojiFlag()
            
            self.countryLabel.text = country.dialCode
            
        }
    }
}
