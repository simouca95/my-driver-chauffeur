//
//  ActivationViewController.swift
//  MyDriver
//
//  Created by sami hazel on 4/6/19.
//  Copyright © 2019 MyDriverKw. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase


class ActivationViewController: UIViewController {
    static let storyboardId = "ActivationViewController"
    var verificationID : String?

    var ref: DatabaseReference!
    let phoneNumberStr = "+21688555555"
    
    // This test verification code is specified for the given test phone number in the developer console.
    let testVerificationCode = "111111"
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        verificationID = UserDefaults.standard.string(forKey: "authVerificationID")
        self.ref = Database.database(url: CURRENT_DB_REF).reference()

    }
    @IBAction func activate(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)

        let credential = PhoneAuthProvider.provider().credential(
            withVerificationID: verificationID!,
            verificationCode: testVerificationCode)
        
        Auth.auth().signInAndRetrieveData(with: credential) { (authResult, error) in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            print(authResult!)
            self.ref.child("Driver").observeSingleEvent(of: .value, with: { (snap) in
                if snap.key == authResult!.user.uid{
                    self.present(storyboard.instantiateViewController(withIdentifier: "mainNVC") as! UINavigationController, animated: true, completion: nil)
                }else{
                    //self.showAlertView(isOneButton: true, title: "You don't have access", cancelButtonTitle: "OK", submitButtonTitle: "OK")

                    self.present(storyboard.instantiateViewController(withIdentifier: RegistrationViewController.storyboardId) as! RegistrationViewController, animated: true, completion: nil)

                }
            }, withCancel: { (error) in
                print(error.localizedDescription)
            })
            // User is signed in
            // ...
        }
        
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
