//
//  Position.swift
//  MyDriver
//
//  Created by sami hazel on 4/14/19.
//  Copyright © 2019 MyDriverKw. All rights reserved.
//

import Foundation
import SwiftyJSON
import FirebaseDatabase
import CoreLocation
import GoogleMaps

class Position : NSObject{
    
    var address : String?

    var latitude : Double?
    var longitude : Double?
    var bearing : Double?
    var marker : GMSMarker!

    
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        address = json!["address"].stringValue

        latitude = json!["lat"].doubleValue
        longitude = json!["lng"].doubleValue
        
        bearing = json!["bearing"].doubleValue
        
        marker = GMSMarker(position: CLLocationCoordinate2D(latitude: latitude!, longitude: longitude!))
        marker.rotation = bearing!
        marker.groundAnchor = CGPoint(x: 0.5, y: 0.5)

        
    }
    
    init(lat : Double , lng : Double, address : String){
        self.address = address

        self.latitude = lat
        self.longitude = lng
        marker = GMSMarker(position: CLLocationCoordinate2D(latitude: latitude!, longitude: longitude!))
        marker.groundAnchor = CGPoint(x: 0.5, y: 0.5)

    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        
        if latitude != nil{
            dictionary["lat"] = latitude
        }
        if longitude != nil{
            dictionary["lng"] = longitude
        }
        
        if bearing != nil{
            dictionary["bearing"] = bearing
        }
        
        if address != nil{
            dictionary["address"] = address
        }

        return dictionary
    }
    
}
