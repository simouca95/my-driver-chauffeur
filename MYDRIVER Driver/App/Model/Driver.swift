//
//  Driver.swift
//  MyDriver
//
//  Created by sami hazel on 4/14/19.
//  Copyright © 2019 MyDriverKw. All rights reserved.
//

import Foundation
import SwiftyJSON
import CoreLocation
import FirebaseAuth
import CoreData

import FirebaseDatabase

class Driver : NSObject{
    
    var uid : String!
    
    var vehicle : Vehicle!
    
    var vehicleId : String!
    
    var agencyId : String?
    
    //var firstName : String!
    //var lastName : String!
    var fullName : String!
    var residence : String!
    var phoneNumber : String!
    var pictureURL : String?
    
    var rating : Float!

    var status : Int!
    var currentPosition : Position?
    var deviceToken : String?
    
    override init() {
        super.init()
    }
    
    convenience init(snap: DataSnapshot) {
        self.init()
        let userDict = JSON(snap.value!)
        self.uid = snap.key
        
        //firstName = userDict["firstName"].stringValue
        //lastName = userDict["lastName"].stringValue
        agencyId = userDict["agencyId"].stringValue
        vehicleId = userDict["vehicleId"].stringValue
        
        fullName = userDict["fullName"].stringValue
        residence = userDict["residence"].stringValue
        pictureURL = userDict["pictureURL"].stringValue
        phoneNumber = userDict["phoneNumber"].stringValue
        
        //deviceToken = userDict["deviceToken"].stringValue
        status = userDict["status"].intValue
        rating = userDict["rating"].float
        let locationJson = userDict["currentPosition"]
        if !locationJson.isEmpty{
            currentPosition = Position(fromJson: locationJson)
        }
        
        self.saveCurrentDriverToLocal()
        
    }
    
    init(phoneNumber : String , fullName: String , id : String , pictureURL : String? = nil) {
        self.uid = id
        self.phoneNumber = phoneNumber
        self.fullName = fullName
        self.status = 0

        let changeRequest = Auth.auth().currentUser?.createProfileChangeRequest()
        changeRequest?.displayName = fullName //+ " " + lastName
        if pictureURL != nil {// , pictureURL!.isValidURL()
            changeRequest?.photoURL = URL(string: pictureURL!)
        }
        changeRequest?.commitChanges { (error) in
            if error != nil {
                print("error update driver fullname and url")
            }
        }
    }
    
    
    static func fetchCurrentDriverFromLocal() -> Driver {
        
        let userId = Auth.auth().currentUser!.uid
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let context = appDelegate.persistentContainer.viewContext
        
        let user = Driver()
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Users")
        request.predicate = NSPredicate(format: "id = %@", userId)
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(request)
            for data in result as! [NSManagedObject] {
                user.uid = userId
                
                user.fullName = data.value(forKey:"firstName") as? String
                user.vehicleId = data.value(forKey:"vehicleId") as? String
                
                user.residence = data.value(forKey:"residence") as? String
                
                user.agencyId = data.value(forKey:"agencyId") as? String
                
                user.rating = data.value(forKey:"rating") as? Float

                if let pic = data.value(forKey:"pictureUrl"){
                    user.pictureURL =  pic as? String
                }
                if let phoneNumber = data.value(forKey:"phoneNumber") as? String{
                    user.phoneNumber = phoneNumber
                }
            }
            
        } catch {
            print(" failed fetch \(error.localizedDescription)")
        }
        return user
    }
    
    
    func saveCurrentDriverToLocal()  {
        
        self.deleteDriver(userId: self.uid)
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let context = appDelegate.persistentContainer.viewContext
        
        let entity = NSEntityDescription.entity(forEntityName: "Users", in: context)
        
        let newUser = NSManagedObject(entity: entity!, insertInto: context)
        
        newUser.setValue(self.uid, forKey: "id")
        
        newUser.setValue(self.fullName, forKey: "firstName")
        newUser.setValue(self.fullName, forKey: "lastName")
        
        newUser.setValue(self.vehicleId, forKey: "vehicleId")
        newUser.setValue(self.agencyId, forKey: "agencyId")

        newUser.setValue(self.rating, forKey: "rating")
        newUser.setValue(self.residence, forKey: "residence")
        
        if let pic = self.pictureURL {
            newUser.setValue(pic, forKey: "pictureUrl")
        }else {
            newUser.setValue(" ", forKey: "pictureUrl")
            
        }
        if let phone = self.phoneNumber {
            newUser.setValue(phone, forKey: "phoneNumber")
        }else {
            newUser.setValue(" ", forKey: "phoneNumber")
            
        }
        
        do {
            try context.save()
        } catch {
            print("Failed saving")
        }
    }
    
    
    func deleteDriver(userId : String) {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        let fetchReq = NSFetchRequest<NSFetchRequestResult>(entityName: "Users")
        fetchReq.predicate = NSPredicate(format: "id = %@", self.uid)
        do {
            let test = try context.fetch(fetchReq)
            if test.count > 0 {
                let objectTodelete = test[0] as! NSManagedObject
                context.delete(objectTodelete)
                do {
                    try context.save()
                } catch  {
                    print(error)
                }
            }
            
        } catch  {
            print(error)
            
        }
    }
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        if uid == nil {
            uid = json["uid"].stringValue
        }
        
        let jsonObject = json["driver"]
        
        fullName = jsonObject["fullName"].stringValue
        phoneNumber = jsonObject["phoneNumber"].stringValue
        pictureURL = jsonObject["pictureURL"].stringValue
        residence = jsonObject["residence"].stringValue
        rating = jsonObject["rating"].float

        let locationJson = jsonObject["currentPosition"]
        
        if !locationJson.isEmpty{

            currentPosition = Position(fromJson: locationJson)
        }
        agencyId = jsonObject["agencyId"].stringValue
        vehicleId = jsonObject["vehicleId"].stringValue

        status = jsonObject["status"].intValue

    }
    
    
    static func acceptRace(courseId : String , vehicleId : String) {
        let ref = Database.database(url: CURRENT_DB_REF).reference()
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ" //Your date format
        
        ref.child("Course").child(courseId).child("acceptedAt").setValue(dateFormatter.string(from: Date()))
        ref.child("Course").child(courseId).child("vehicleId").setValue(vehicleId)
        ref.child("Course").child(courseId).child("driverId").setValue(Auth.auth().currentUser!.uid)
        ref.child("Course").child(courseId).child("status").setValue("accepted")
        
        ref.child("Driver").child(Auth.auth().currentUser!.uid).child("status").setValue(2)

    }
    
    static func updateDeviceToken(_ driverId : String , _ deviceToken : String) {
        
        let ref = Database.database(url: CURRENT_DB_REF).reference()
        
        ref.child("Driver").child(driverId).child("deviceToken").setValue(deviceToken)
    }
    
    static func updateLocation(position : CLLocation) {
        
        let ref = Database.database(url: CURRENT_DB_REF).reference()
//        ref.child("Driver").child(Auth.auth().currentUser!.uid).child("currentPosition").updateChildValues(["lat" : position.coordinate.latitude,
//                                                                                                            "lng" : position.coordinate.longitude,
//                                                                                                            "bearing" : position.course])
        
        ref.child("Driver").child(Auth.auth().currentUser!.uid).child("currentPosition").child("lat").setValue(position.coordinate.latitude)
        ref.child("Driver").child(Auth.auth().currentUser!.uid).child("currentPosition").child("lng").setValue(position.coordinate.longitude)
        ref.child("Driver").child(Auth.auth().currentUser!.uid).child("currentPosition").child("bearing").setValue(position.course)
    }

    static func connectDriver(status : Int) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ" //Your date format
        dateFormatter.locale = Locale(identifier: "en")
        
        let ref = Database.database(url: CURRENT_DB_REF).reference()
        
        ref.child("Driver").child(Auth.auth().currentUser!.uid).child("status").setValue(status)
        
        ref.child("Driver").child(Auth.auth().currentUser!.uid).child("history").child(dateFormatter.string(from: Date())).setValue(status)
    }
    
    static func fetchCurrentDriver(completionBlock: @escaping (DataSnapshot?)->Void ){
        
        let ref = Database.database(url: CURRENT_DB_REF).reference()
        
        ref.child("Driver").child(Auth.auth().currentUser!.uid).observeSingleEvent(of : .value ,with : completionBlock)
    }
    
    static func getNearbyDrivers(json : JSON ) -> [Driver]? {
        
        var nearbyCars = [Driver]()
        
        for car in json.arrayValue {
            let vehicle = Vehicle(fromJsonWithUID: car)
            let driver = Driver(fromJson: car)
            driver.vehicle = vehicle
            nearbyCars.append(driver)
        }
        return nearbyCars
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        
        if fullName != nil{
            dictionary["fullName"] = fullName
        }else {
            dictionary["fullName"] = ""
        }
        
        if residence != nil{
            dictionary["residence"] = residence
        }else{
            dictionary["residence"] = ""
        }
        
        if currentPosition != nil{
            dictionary["currentPosition"] = ["lat": currentPosition!.latitude , "lng" : currentPosition!.longitude , "bearing" : currentPosition!.bearing ]
        }else {
            dictionary["currentPosition"] = ["lat": 0 , "lng" : 0 , "bearing" : 0 ]
        }

        
        if vehicleId != nil{
            dictionary["vehicleId"] = vehicleId
        }else {
            dictionary["vehicleId"] = ""
        }

        
        if agencyId != nil{
            dictionary["agencyId"] = agencyId
        }else {
            dictionary["agencyId"] = ""
        }

        
        if pictureURL != nil{
            dictionary["pictureURL"] = pictureURL!
        }else {
            dictionary["pictureURL"] = ""
        }


        if status != nil{
            dictionary["status"] = status
        }else {
            dictionary["status"] = ""
        }

        
        if phoneNumber != nil{
            dictionary["phoneNumber"] = phoneNumber
        }else {
            dictionary["phoneNumber"] = ""
        }


        return dictionary
    }
    
}
