//
//  Vehicle.swift
//  MyDriver
//
//  Created by sami hazel on 4/14/19.
//  Copyright © 2019 MyDriverKw. All rights reserved.
//

import Foundation
import SwiftyJSON
import FirebaseDatabase
import CoreLocation

class Vehicle : NSObject{
    
    var uid : String!
    
    var agencyId : String?
    var brand : String!
    var color : String!
    var category : String!
    var registrationNumber : String!
    var vin : String!

    var capacity : Int!
    
    var driver : Driver!
    
    
    convenience init(snap: DataSnapshot) {
        
        let userDict = snap.value as! JSON
        self.init(fromJson: userDict)
        self.uid = snap.key

    }
    
    
    convenience init(fromJsonWithUID json: JSON!){
        self.init(fromJson: json["car"])
        
        if json.isEmpty{
            return
        }
        uid = json["carId"].stringValue
        
    }
    
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        
        brand = json["brand"].stringValue
        capacity = json["capacity"].int
        
        agencyId = json["agencyId"].stringValue
        color = json["color"].stringValue
        
        category = json["category"].stringValue
        registrationNumber = json["plateNumber"].stringValue
        vin = json["vin"].stringValue

        
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        
        if capacity != nil{
            dictionary["capacity"] = capacity
        }
        
        if brand != nil{
            dictionary["brand"] = brand
        }
        
        if agencyId != nil{
            dictionary["agencyId"] = agencyId
        }
        if color != nil{
            dictionary["color"] = color
        }
        
        if category != nil{
            dictionary["category"] = category
        }
        
        if registrationNumber != nil{
            dictionary["plateNumber"] = registrationNumber
        }
        
        if vin != nil{
            dictionary["vin"] = vin
        }
        
        return dictionary
    }
        

    
}
