//
//  User.swift
//  MyDriver
//
//  Created by sami hazel on 4/14/19.
//  Copyright © 2019 MyDriverKw. All rights reserved.
//

import Foundation
import FirebaseDatabase
import SwiftyJSON
import FirebaseAuth

class User: NSObject {
    
    var email : String!
    var firstName : String!
    var deviceToken : String!
    var lastName : String!
    var pictureURL : String!
    var uid : String!
    var favoriteAddress : [FavoriteAddress]?
    var phoneNumber : String!

    init(snap: DataSnapshot) {
        
        uid = snap.key

        let userDict = snap.value as! [String: Any]

        email = userDict["email"] as? String
        firstName = userDict["firstName"] as? String
        lastName = userDict["lastName"] as? String
        pictureURL = userDict["pictureURL"] as? String
        phoneNumber = userDict["phoneNumber"] as? String

        favoriteAddress = [FavoriteAddress]()
        if let userDict = userDict["favorites"] {
            let favoriteAddressArray = JSON(userDict).dictionaryValue
            for fav in favoriteAddressArray{
                let value = FavoriteAddress(fromJson: fav.value)
                value.uid = fav.key
                favoriteAddress!.append(value)
            }
        }
    }
    
    init( id : String , email : String , firstname: String, lastname: String , phoneNumber : String , pictureURL : String? = nil) {
        self.uid = id
        self.email = email
        self.firstName = firstname
        self.lastName = lastname
        self.phoneNumber = phoneNumber
        
        Auth.auth().currentUser?.updateEmail(to: email) { (error) in
            if error != nil {
                print("error update user email")
            }
        }
        
        let changeRequest = Auth.auth().currentUser?.createProfileChangeRequest()
        
        changeRequest?.displayName = firstname + " " + lastname
        
        if pictureURL != nil  { //, pictureURL!.isValidURL()
            changeRequest?.photoURL = URL(string: pictureURL!)
        }
        changeRequest?.commitChanges { (error) in
            if error != nil {
                print("error update user fullname and url")
            }
        }
    }
    
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        
        if email != nil{
            dictionary["email"] = email
        }
        
        if lastName != nil{
            dictionary["lastName"] = lastName
        }
        
        if firstName != nil{
            dictionary["firstName"] = firstName
        }
        
        if phoneNumber != nil{
            dictionary["phoneNumber"] = phoneNumber
        }
        
        if pictureURL != nil{
            
            dictionary["pictureURL"] = pictureURL
        }else {
            
            dictionary["pictureURL"] = ""
        }
        
        return dictionary
    }

    
}
