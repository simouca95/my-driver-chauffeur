//
//  FavoriteAddress.swift
//  MyDriver
//
//  Created by sami hazel on 4/22/19.
//  Copyright © 2019 MyDriverKw. All rights reserved.
//

import Foundation
import SwiftyJSON
import FirebaseDatabase
import CoreLocation
import GoogleMaps

class FavoriteAddress : NSObject{
    
    var name : String?
    var latitude : Double?
    var longitude : Double?
    var type : String?
    var uid : String?
    
    
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        name = json!["address"].stringValue
        
        latitude = json!["lat"].doubleValue
        longitude = json!["lng"].doubleValue
        type = json!["cat"].stringValue
    }
    
    
    init(name : String , lat : Double , lng : Double , type : String){
        self.latitude = lat
        self.longitude = lng
        self.name = name
        self.type = type
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        
        if latitude != nil{
            dictionary["lat"] = latitude
        }
        
        if longitude != nil{
            dictionary["lng"] = longitude
        }
        
        if name != nil{
            dictionary["address"] = name
        }
        
        if type != nil{
            dictionary["cat"] = type
        }
        
        return dictionary
    }
    
}
