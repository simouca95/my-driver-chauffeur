//
//  Facture.swift
//  MYDRIVER Driver
//
//  Created by sami hazel on 6/26/19.
//  Copyright © 2019 MyDriverKw. All rights reserved.
//

import Foundation
import SwiftyJSON
import FirebaseDatabase
import CoreLocation
import FirebaseAuth

class Facture : NSObject{
    
    var uid : String!
    
    var driverId : String?
    var clientId : String!
    var vehicleId : String?
    
    
    var tripDate : Date!

    var addressDestination : String?
    var addresspickUp : String?

    var finalCost : Float?
    
    var status : String!

    override init() {
        super.init()
    }
    
    convenience init(snap: DataSnapshot) {
        self.init()
        let userDict = JSON(snap.value!)
        self.uid = snap.key
        
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ" //Your date format
        
        tripDate = dateFormatter.date(from: userDict["tripDate"].stringValue)

        
        clientId = userDict["clientId"].stringValue
        vehicleId = userDict["vehicleId"].stringValue
        driverId = userDict["driverId"].stringValue


        
        finalCost = userDict["finalCost"].floatValue
        
        status = userDict["status"].stringValue
        
        addressDestination = userDict["to"].stringValue
        addresspickUp = userDict["from"].stringValue

    }
    
    
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    convenience init(fromJson json: JSON!){
        
        self.init()
        
        if json.isEmpty{
            return
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ" //Your date format
        
        tripDate = dateFormatter.date(from: json["tripDate"].stringValue)

        
        clientId = json["clientId"].stringValue
        vehicleId = json["vehicleId"].stringValue
        driverId = json["driverId"].stringValue
        

        finalCost = json["finalCost"].floatValue
        
        status = json["status"].stringValue
        
        addressDestination = json["to"].stringValue
        addresspickUp = json["from"].stringValue
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ" //Your date format
        
        
        var dictionary = [String:Any]()
        
        if tripDate != nil{
            dictionary["tripDate"] = dateFormatter.string(from: tripDate)
        }else {
            
            dictionary["tripDate"] = ""
        }

        
        
        if clientId != nil{
            dictionary["clientId"] = clientId
        }else {
            
            dictionary["clientId"] = ""
        }
        
        
        if driverId != nil{
            dictionary["driverId"] = driverId
        }else {
            
            dictionary["driverId"] = ""
        }
        
        if vehicleId != nil{
            dictionary["vehicleId"] = vehicleId
        }else {
            
            dictionary["vehicleId"] = ""
        }
        
        

        
        if finalCost != nil{
            dictionary["finalCost"] = finalCost
        }else {
            
            dictionary["finalCost"] = 0
        }
        
        if addresspickUp != nil {
            dictionary["from"] = addresspickUp
        }else {
            dictionary["from"] = ""

        }
        
        if addressDestination != nil {
            dictionary["to"] = addressDestination
        }else {
            dictionary["to"] = ""
            
        }
        
        if status != nil{
            dictionary["status"] = status
        }
        
        return dictionary
    }
    
    
    static func fetchMyFactures(_ completionBlock: @escaping (DataSnapshot)->Void , _ errorBlock : @escaping (Error?)->Void ) {
        let ref = Database.database(url: CURRENT_DB_REF).reference()
        ref.child("Facture").queryOrdered(byChild: "driverId").queryEqual(toValue: Auth.auth().currentUser!.uid).observeSingleEvent(of: .value, with: completionBlock, withCancel: errorBlock)
    }
    
}
