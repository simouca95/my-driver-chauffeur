//
//  Course.swift
//  MyDriver
//
//  Created by sami hazel on 4/14/19.
//  Copyright © 2019 MyDriverKw. All rights reserved.
//

import Foundation
import SwiftyJSON
import FirebaseDatabase
import CoreLocation
import FirebaseAuth


struct WaitingTimeStruct {
    var duration : Int!
    var status : String!
}

class Course : NSObject{
    
    var uid : String!

    var driverId : String?
    var clientId : String!
    var vehicleId : String?

    
    var createdAt : Date!
    var startedAt : Date?
    var finishedAt : Date?
    var acceptedAt : Date?
    var arrivedAt : Date?
    
    var destination : Position!
    var pickUpPoint : Position!

    var classCar : String!
    var estimatedCost : Double?
    var finalCost : Double?
    
    var waitingTime : [WaitingTimeStruct]?
    var estimatedTimeArrival : Int!
    var rating : Int?
    var status : String!
    var initialDistance : Int!
    
    var courseHasChanged : Int!

    
    override init() {
        super.init()
    }
    
    convenience init(snap: DataSnapshot) {
        self.init()
        let userDict = JSON(snap.value!)
        self.uid = snap.key

        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ" //Your date format
        
        startedAt = dateFormatter.date(from: userDict["startedAt"].stringValue)
        createdAt = dateFormatter.date(from: userDict["createdAt"].stringValue)
        finishedAt = dateFormatter.date(from: userDict["finishedAt"].stringValue)
        acceptedAt = dateFormatter.date(from: userDict["acceptedAt"].stringValue)
        arrivedAt = dateFormatter.date(from: userDict["arrivedAt"].stringValue)

        classCar = userDict["classCar"].stringValue
        clientId = userDict["clientId"].stringValue
        vehicleId = userDict["vehicleId"].stringValue
        
        let destinationJson = userDict["destination"]
        if !destinationJson.isEmpty{
            destination = Position(fromJson: destinationJson)
        }
        let pickUpPointJson = userDict["pickUpPoint"]
        if !pickUpPointJson.isEmpty{
            pickUpPoint = Position(fromJson: pickUpPointJson)
        }
        
        initialDistance = userDict["initialDistance"].int
        driverId = userDict["driverId"].stringValue
        estimatedCost = userDict["ec"].doubleValue
        finalCost = userDict["fc"].doubleValue
        estimatedTimeArrival = userDict["eta"].int
        rating = userDict["rating"].intValue
        status = userDict["status"].stringValue
        
        courseHasChanged = userDict["courseHasChanged"].intValue

        if let json = userDict["wt"].dictionary{
            waitingTime = [WaitingTimeStruct]()
            
            json.forEach { (arg) in
                let value = arg.value
                let wtStruct = WaitingTimeStruct(duration: value.dictionaryValue["duration"]?.intValue, status: value.dictionaryValue["status"]?.stringValue)
                if waitingTime == nil {
                    waitingTime = [wtStruct]
                }else{
                    waitingTime!.append(wtStruct)
                }
            }
            
        }
        
    }
    
    
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    convenience init(fromJson json: JSON!){
        
        self.init()

        if json.isEmpty{
            return
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ" //Your date format
        
        startedAt = dateFormatter.date(from: json["startedAt"].stringValue)
        createdAt = dateFormatter.date(from: json["createdAt"].stringValue)
        finishedAt = dateFormatter.date(from: json["finishedAt"].stringValue)
        acceptedAt = dateFormatter.date(from: json["acceptedAt"].stringValue)
        arrivedAt = dateFormatter.date(from: json["arrivedAt"].stringValue)

        classCar = json["classCar"].stringValue
        clientId = json["clientId"].stringValue
        vehicleId = json["vehicleId"].stringValue
        driverId = json["driverId"].stringValue
        
        let destinationJson = json["destination"]
        if !destinationJson.isEmpty{
            destination = Position(fromJson: destinationJson)
        }
        let pickUpPointJson = json["pickUpPoint"]
        if !pickUpPointJson.isEmpty{
            pickUpPoint = Position(fromJson: pickUpPointJson)
        }

        initialDistance = json["initialDistance"].int
        estimatedCost = json["ec"].doubleValue
        finalCost = json["fc"].doubleValue
        estimatedTimeArrival = json["eta"].int
        rating = json["rating"].intValue
        status = json["status"].stringValue
        courseHasChanged = json["courseHasChanged"].intValue

        if let json = json["wt"].dictionary{
            waitingTime = [WaitingTimeStruct]()
            
            json.forEach { (arg) in
                let value = arg.value
                let wtStruct = WaitingTimeStruct(duration: value.dictionaryValue["duration"]?.intValue, status: value.dictionaryValue["status"]?.stringValue)
                if waitingTime == nil {
                    waitingTime = [wtStruct]
                }else{
                    waitingTime!.append(wtStruct)
                }
            }
            
        }else{
            waitingTime = []
        }
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ" //Your date format
        
        
        var dictionary = [String:Any]()
        
        if acceptedAt != nil{
            dictionary["acceptedAt"] = dateFormatter.string(from: acceptedAt!)
        }else {
            
           dictionary["acceptedAt"] = ""
        }
        
        if arrivedAt != nil{
            dictionary["arrivedAt"] = dateFormatter.string(from: arrivedAt!)
        }else {
            
            dictionary["arrivedAt"] = ""
        }
        
        if finishedAt != nil{
            dictionary["finishedAt"] = dateFormatter.string(from: finishedAt!)
        }else {
            
            dictionary["finishedAt"] = ""
        }

        if createdAt != nil{
            dictionary["createdAt"] = dateFormatter.string(from: createdAt)
        }else {
            
            dictionary["createdAt"] = ""
        }

        if startedAt != nil{
            dictionary["startedAt"] = dateFormatter.string(from: startedAt!)
        }else {
            
            dictionary["startedAt"] = ""
        }

        if estimatedTimeArrival != nil{
            dictionary["eta"] = estimatedTimeArrival
        }else {
            
            dictionary["eta"] = 0
        }
        
        if initialDistance != nil{
            dictionary["initialDistance"] = initialDistance
        }else {
            
            dictionary["initialDistance"] = 0
        }

        if classCar != nil{
            dictionary["classCar"] = classCar
        }
        
        if clientId != nil{
            dictionary["clientId"] = clientId
        }else {
            
            dictionary["clientId"] = ""
        }

        
        if driverId != nil{
            dictionary["driverId"] = driverId
        }else {
            
            dictionary["driverId"] = ""
        }

        if vehicleId != nil{
            dictionary["vehicleId"] = vehicleId
        }else {
            
            dictionary["vehicleId"] = ""
        }

        
        if estimatedCost != nil{
            dictionary["ec"] = estimatedCost
        }else {
            
            dictionary["ec"] = 0
        }

        if finalCost != nil{
            dictionary["fc"] = finalCost
        }else {
            
            dictionary["fc"] = 0
        }
        
        if pickUpPoint != nil{
            dictionary["pickUpPoint"] = ["lat": pickUpPoint.latitude! , "lng" : pickUpPoint.longitude!, "bearing" : pickUpPoint.bearing!, "address" : pickUpPoint.address! ]
            
        }else {
            dictionary["pickUpPoint"] = ["lat": 0 , "lng" : 0 , "bearing" : 0 , "address" : ""]
        }
        
        if destination != nil{
            dictionary["destination"] = ["lat": destination.latitude! , "lng" : destination.longitude!, "bearing" : destination.bearing!, "address" : destination.address! ]
        }else {
            dictionary["destination"] = ["lat": 0 , "lng" : 0 , "bearing" : 0 , "address" : ""]
        }
        
        if rating != nil{
            dictionary["rating"] = rating
        }else {
            
            dictionary["rating"] = 0
        }
        

        if status != nil{
            dictionary["status"] = status
        }
        
        if courseHasChanged != nil{
            dictionary["courseHasChanged"] = courseHasChanged
        }else {
            dictionary["courseHasChanged"] = 0

        }
        
        if waitingTime != nil{
            dictionary["wt"] = waitingTime
        }else {
            
            dictionary["wt"] = 0
        }
        
        return dictionary
    }
    

    func deleteWT() {
        let ref = Database.database(url: CURRENT_DB_REF).reference()
        ref.child("Course").child(self.uid).child("wt").removeValue()

    }
    func setWaitingTime(waitingTime : WaitingTimeStruct) {
        
        let ref = Database.database(url: CURRENT_DB_REF).reference()
        let wtStruct = ["duration" : waitingTime.duration , "status" : waitingTime.status] as [String : Any]
        ref.child("Course").child(self.uid).child("wt").childByAutoId().setValue(wtStruct)
        
    }
    
    func getCustomerPromoWallet( completionBlock: @escaping (DataSnapshot?)->Void, cancelBlock: @escaping (Error?)->Void) {
        let ref = Database.database(url: CURRENT_DB_REF).reference()
        ref.child("User").child(self.clientId).child("promoWallet").observeSingleEvent(of: .value, with: completionBlock, withCancel: cancelBlock)
    }
    
    
    static func fetchCurrentTripDetails(courseId : String, completionBlock: @escaping (DataSnapshot?)->Void) {
        let ref = Database.database(url: CURRENT_DB_REF).reference()
        ref.child("Course").child(courseId).observeSingleEvent(of: .value, with: completionBlock)
    }
    
    static func getTripDetails(courseId : String, completionBlock: @escaping (DataSnapshot?)->Void) -> UInt {
        var handleUINT : UInt!
        let ref = Database.database(url: CURRENT_DB_REF).reference()
        handleUINT =  ref.child("Course").child(courseId).observe(.value, with: completionBlock)
        //ref.child(courseId).observeSingleEvent(of: .value, with: completionBlock)
        return handleUINT
    }
    
    static func removeCourseObserver(courseId : String , handle : UInt){
        
        let ref = Database.database(url: CURRENT_DB_REF).reference()
        ref.child("Course").child(courseId).removeObserver(withHandle: handle)
    }
    
    static func tripStarted(tripId : String) {
        
        let ref = Database.database(url: CURRENT_DB_REF).reference()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ" //Your date format
        
        ref.child("Course").child(tripId).child("startedAt").setValue(dateFormatter.string(from: Date()))
        ref.child("Course").child(tripId).child("status").setValue("started")

    }
    
    func tripFinished( finalCostAux : Double) {
        var promo : Double = 0
        let ref = Database.database(url: CURRENT_DB_REF).reference()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ" //Your date format
        let fc = ((Double(finalCostAux) * 1000).rounded(.up)) / 1000
        self.getCustomerPromoWallet(completionBlock: { (snap) in
            if let snap = snap , snap.exists(){
                if snap.value as! Float > 0 {
                    promo = snap.value as! Double
                }
            }

            if promo > fc {
                self.finalCost = 0
                ref.child("Course").child(self.uid).child("fc").setValue(0)
                self.updateCustomerPromoWallet(value: ((promo - fc) * 1000).rounded(.up) / 1000)
            }else {
                self.finalCost =  ( (fc - promo) * 1000).rounded(.up) / 1000

                ref.child("Course").child(self.uid).child("fc").setValue( ( (fc - promo) * 1000).rounded(.up) / 1000)
                self.updateCustomerPromoWallet(value: 0)
            }
            ref.child("Course").child(self.uid).child("finishedAt").setValue(dateFormatter.string(from: Date()))
            ref.child("Course").child(self.uid).child("status").setValue("finished")
            
        }) { (error) in
            print(error!.localizedDescription)
            self.finalCost =  fc
            ref.child("Course").child(self.uid).child("fc").setValue(fc)
            ref.child("Course").child(self.uid).child("finishedAt").setValue(dateFormatter.string(from: Date()))
            ref.child("Course").child(self.uid).child("status").setValue("finished")
        }
    }
    
    static func arrivedToPickUpPoint(tripId : String) {
        
        let ref = Database.database(url: CURRENT_DB_REF).reference()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ" //Your date format
        
        ref.child("Course").child(tripId).child("arrivedAt").setValue(dateFormatter.string(from: Date()))
        ref.child("Course").child(tripId).child("status").setValue("arrived")
    }
    
    func tripHasChanged() {
        let ref = Database.database(url: CURRENT_DB_REF).reference()
        ref.child("Course").child(self.uid).child("courseHasChanged").setValue(1)

    }
    
    func tripCanceledByDriver(reason : String) {
        let ref = Database.database(url: CURRENT_DB_REF).reference()
        ref.child("Course").child(self.uid).child("status").setValue("canceled")
        ref.child("Course").child(self.uid).child("cancellationReason").setValue(reason)

    }

    func customerDidPay(with : String) {
        let ref = Database.database(url: CURRENT_DB_REF).reference()
        ref.child("Course").child(self.uid).child("paymentMethod").setValue(with)
        
    }
    
    func updateCustomerPromoWallet(value : Double) {
        let ref = Database.database(url: CURRENT_DB_REF).reference()
        ref.child("User").child(self.clientId).child("promoWallet").setValue(value)
        
    }

    
}
