//
//  CreditCard.swift
//  MyDriver
//
//  Created by sami hazel on 4/14/19.
//  Copyright © 2019 MyDriverKw. All rights reserved.
//

import Foundation
import FirebaseDatabase
import SwiftyJSON

class CreditCard: NSObject  {

    var cvv : String!
    var cardNumber : String!
    var country : String!
    var experationDate : String!
    var type : String!
    var uid : String!
    
    init(snap: DataSnapshot) {
        
        let userDict = snap.value as! [String: Any]

        cvv = userDict["cvv"] as! String
        cardNumber = userDict["cardNumber"] as! String
        country = userDict["country"] as! String
        experationDate = userDict["exp"] as! String
        type = userDict["issuingNetwork"] as! String

    }
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        cvv = json["cvv"].stringValue

        cardNumber = json["cardNumber"].stringValue
        country = json["country"].stringValue
        experationDate = json["country"].stringValue
        type = json["country"].stringValue

    }
    
}
