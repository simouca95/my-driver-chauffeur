//
//  AddressDetailViewController.swift
//  MYDRIVER Driver
//
//  Created by sami hazel on 6/29/19.
//  Copyright © 2019 MyDriverKw. All rights reserved.
//

import UIKit

class AddressDetailViewController: UIViewController {

    static let storyboardId = "AddressDetailViewController"

    @IBOutlet weak var address: UILabel!
    var addressStr = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let screenSize = UIScreen.main.bounds.size
        
        let popupWidth = screenSize.width - 40
        
        let popupHeight : CGFloat = 350
        
        let popupSize = CGSize(width: popupWidth, height: popupHeight)
        
        self.contentSizeInPopup = popupSize
        //popupController?.backgroundView?.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(self.dismissPopup)))
        
        self.popupController?.containerView.layer.cornerRadius = 10
        self.popupController?.containerView.clipsToBounds = true
        self.popupController?.containerView.backgroundColor = UIColor.clear
        self.popupController?.hidesCloseButton = false
        self.popupController?.navigationBarHidden = false
        
        self.popupController?.backgroundView?.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(self.dismissPopup)))

        self.address.text = addressStr
    }
    
    @objc func dismissPopup(){
        
        self.popupController?.dismiss()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
