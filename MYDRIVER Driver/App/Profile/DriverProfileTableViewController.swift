//
//  DriverProfileTableViewController.swift
//  MYDRIVER Driver
//
//  Created by sami hazel on 6/17/19.
//  Copyright © 2019 MyDriverKw. All rights reserved.
//

import UIKit
import FirebaseAuth
import SDWebImage
import SwiftyJSON
import FirebaseStorage

class DriverProfileTableViewController: UITableViewController {

    static let storyboardId = "DriverProfileTableViewController"

    @IBOutlet weak var driverResidence: UILabel!
    @IBOutlet weak var driverPhoneNumber: UILabel!
    @IBOutlet weak var driverEmail: UILabel!
    @IBOutlet weak var tripsByDay: UILabel!
    @IBOutlet weak var driverToPay: UILabel!
    
    @IBOutlet weak var driverByMonth: UILabel!

    @IBOutlet weak var driverName: UILabel!
    @IBOutlet weak var driverPicture: UIImageView!
    
    @IBOutlet weak var driverDetailsView: UIView!
    var toPay : Float = 0
    var incomeThisMonth : Float = 0
    var nbTripsThisDay = 0
    
    var thisDayFactures = [Facture]()

    var currentDriver : Driver!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        driverPicture.borderWithCornder(raduis: driverPicture.frame.height / 2, borderColor: UIColor.black.cgColor, borderWidth: 1)
        
        self.currentDriver = Driver.fetchCurrentDriverFromLocal()
        
        self.driverName.text = self.currentDriver.fullName
        self.driverPhoneNumber.text = self.currentDriver.phoneNumber
        self.driverResidence.text = self.currentDriver.residence
        
        if let pic = self.currentDriver.pictureURL{
            
            if pic.isValidURL(){
                self.driverPicture.sd_setImage(with: URL(string: pic), completed: { (image, error, cache, url) in
                    if error == nil {
                        self.driverPicture.image = image
                    }
                })
            }else if pic.count > 0{
                let storageRef = Storage.storage().reference()
                let profileImagesRef = storageRef.child("ProfilePictures/\(pic)")
                
                // You can also access to download URL after upload.
                profileImagesRef.downloadURL { (url, error) in
                    guard let downloadURL = url else {
                        // Uh-oh, an error occurred!
                        return
                    }
                    self.driverPicture.sd_setImage(with: downloadURL, completed: { (image, error, cache, url) in
                        if error == nil {
                            self.driverPicture.image = image
                        }
                    })
                    
                }
            }
        }
        getDriverHistory()
    }

    func getDriverHistory() {
        self.showSpinner(onView: self.driverDetailsView)
        Facture.fetchMyFactures({ (snap) in
            self.removeSpinner()
            if snap.exists(){
                
                let date = Date()
                
                let factures = JSON(snap.value!).dictionaryValue
                for fac in factures {
                    let facture = Facture(fromJson: fac.value)
                    facture.uid = fac.key
                    
                    if facture.status == "unpaid"{
                        self.toPay += facture.finalCost!
                    }
                    
                    if facture.tripDate.year == date.year ,facture.tripDate.month == date.month{
                        self.incomeThisMonth += facture.finalCost!
                        if facture.tripDate.day == date.day{
                            self.thisDayFactures.append(facture)
                        }
                    }
                }

                self.tripsByDay.text = "\(self.thisDayFactures.count)"
                self.driverByMonth.text = "\(((Double(self.incomeThisMonth) * 1000).rounded(.up)) / 1000) KWD"
                self.driverToPay.text = "\(((Double(self.toPay) * 1000).rounded(.up)) / 1000) KWD"
            }else {
                
            }
            
        }) { (error) in
            if let error = error{
                print(error.localizedDescription)
                self.handleError(error)
                self.removeSpinner()
            }
        }
    }
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 5
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 200
        }
        if indexPath.row == 1 {
            return 100
        }
        return 60
    }

    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
