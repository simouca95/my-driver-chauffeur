//
//  AppDelegate.swift
//  MYDRIVER Driver
//
//  Created by sami hazel on 4/21/19.
//  Copyright © 2019 MyDriverKw. All rights reserved.
//

import UIKit
import Firebase
import IQKeyboardManagerSwift
import GoogleMaps
import UserNotifications
import FirebaseMessaging
import SwiftyJSON
import CoreData
import NotificationBannerSwift

let googleApiKey = "AIzaSyDn3vWzI2h6bDRNZ9rCKqmvw3MHE9X3LZs"
let matrixApiKey = "AIzaSyDSj8fDdsr2ff4vlhFupag0azI3aVazfzw"
let directionsApiKey = "AIzaSyCm2kW7TPsr8F4gWWKy7blOIteUkvDOmHU"

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let gcmMessageIDKey = "gcm.message_id"
    
    var ref: DatabaseReference!



    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.sharedManager().shouldResignOnTouchOutside = true
        
        GMSServices.provideAPIKey(googleApiKey)
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {success, error in
                    if error == nil {
                        if success == true {
                            print("Permission granted")
                            DispatchQueue.main.async {
                                // In case you want to register for the remote notifications
                                let application = UIApplication.shared
                                application.registerForRemoteNotifications()
                            }
                        }
                        else {
                            print("Permission denied")
                        }
                    }else {
                        print(error)
                    }
            })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        loadPrices()

        
        if #available(iOS 10.0, *) {
            let center = UNUserNotificationCenter.current()
            center.removeAllPendingNotificationRequests() // To remove all pending notifications which are not delivered yet but scheduled.
            center.removeAllDeliveredNotifications() // To remove all delivered notifications
        }
        return true
    }
    
    // MARK: - Load Config
    func loadPrices(){
        let ref = Database.database(url: CURRENT_DB_REF).reference().child("config")
        ref.keepSynced(true)
        ref.observe(DataEventType.value) { (snap) in
            self.saveConfigsToLocal(snap: snap)
        }
    }
    //save config to local
    func saveConfigsToLocal(snap : DataSnapshot) {
        //save to local
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        // delete saved configs
        let fetchReq = NSFetchRequest<NSFetchRequestResult>(entityName: "Config")
        do {
            let test = try context.fetch(fetchReq)
            if test.count > 0{
                let objectTodelete = test[0] as! NSManagedObject
                context.delete(objectTodelete)
                print("delete saved configs")
                do {
                    try context.save()
                } catch  {
                    print(error)
                }
            }
        } catch  {
            print(error)
        }
        
        ///delete saved prices
        let fetchPricesReq = NSFetchRequest<NSFetchRequestResult>(entityName: "Prices")
        do {
            let test = try context.fetch(fetchPricesReq)
            if test.count > 0{
                for objectTodelete in test as! [NSManagedObject]{
                    context.delete(objectTodelete)
                }
                do {
                    try context.save()
                    print("delete saved prices")
                    
                } catch  {
                    print(error)
                }
            }
        } catch  {
            print(error)
        }
        
        //save new prices
        let entityPrices = NSEntityDescription.entity(forEntityName: "Prices", in: context)
        
        if let _ =  snap.childSnapshot(forPath: "prices").value{
            
            for item in snap.childSnapshot(forPath: "prices").children.allObjects as! [DataSnapshot]{
                let prices = NSManagedObject(entity: entityPrices!, insertInto: context)
                prices.setValue(item.key, forKey: "category")
                
                prices.setValue((item.value as? NSDictionary)!.value(forKey: "pricePerKm")!, forKey: "pricePerKm")
                prices.setValue((item.value as? NSDictionary)!.value(forKey: "pricePerMin")!, forKey: "pricePerMin")
                prices.setValue((item.value as? NSDictionary)!.value(forKey: "baseFare")!, forKey: "baseFare")
                prices.setValue((item.value as? NSDictionary)!.value(forKey: "waitingCost")!, forKey: "waitingCost")
                prices.setValue((item.value as? NSDictionary)!.value(forKey: "priceTenMin")!, forKey: "priceTenMin")
                do {
                    try context.save()
                    print("save new prices")
                    
                } catch {
                    print("Failed saving")
                }
            }
        }
        
        //save new configs
        
        let entityConfig = NSEntityDescription.entity(forEntityName: "Config", in: context)
        let config = NSManagedObject(entity: entityConfig!, insertInto: context)
        
        if let trafficModelSnap = snap.childSnapshot(forPath: "trafficModel").value{
            config.setValue(trafficModelSnap,forKey: "trafficModel")
        }

        
        do {
            try context.save()
            print("save new configs")
            
        } catch {
            print("Failed saving")
        }
    }
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "Model")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    func getNotification(from userInfo: [AnyHashable : Any]) -> NotificationStruct {
        print("userInfo: ", userInfo)
        
        var alert = ""
        
        var type = 0
        
        var pickUpAddress = ""
        
        var pickUpLatitude : Double = 0
        
        var pickUpLongitude : Double = 0
        
        var destinationAddress = ""
        
        var destinationLatitude : Double = 0
        
        var destinationLongitude : Double = 0
        
        var initialDistance : Int = 0
        
        var estimatedCost : Int = 0
        
        var estimatedTime : Int = 0
        
        var fullName = ""
        
        var sentAt = ""
        
        var phoneNumber = ""
        
        var pictureURL = ""
        
        var tripId = ""
        
        if let aps = userInfo["aps"] as? NSDictionary{
            
            if let a = aps["alert"] as? NSDictionary{
                
                if let body = a.value(forKey: "body") as? String {
                    
                    alert = body
                }
            }
        }


        if let params = JSON(userInfo["data"]!).rawString()!.convertToDictionary(), let t = params.value(forKey: "type") as? Int {
            type = t
        }
        
        if let params = JSON(userInfo["data"]!).rawString()!.convertToDictionary(), let objectId = params.value(forKey: "pickUpAddress") as? String{
            pickUpAddress = objectId
        }
        
        if let params = JSON(userInfo["data"]!).rawString()!.convertToDictionary(), let objectId = params.value(forKey: "pickUpLatitude") as? Double{
            pickUpLatitude = objectId
        }
        
        if let params = JSON(userInfo["data"]!).rawString()!.convertToDictionary(), let objectId = params.value(forKey: "pickUpLongitude") as? Double{
            pickUpLongitude = objectId
        }
        
        if let params = JSON(userInfo["data"]!).rawString()!.convertToDictionary(), let objectId = params.value(forKey: "destinationAddress") as? String{
            destinationAddress = objectId
        }
        if let params = JSON(userInfo["data"]!).rawString()!.convertToDictionary(), let objectId = params.value(forKey: "destinationLatitude") as? Double{
            destinationLatitude = objectId
        }
        if let params = JSON(userInfo["data"]!).rawString()!.convertToDictionary(), let objectId = params.value(forKey: "destinationLongitude") as? Double{
            destinationLongitude = objectId
        }
        if let params = JSON(userInfo["data"]!).rawString()!.convertToDictionary(), let objectId = params.value(forKey: "initialDistance") as? Int{
            initialDistance = objectId
        }
        if let params = JSON(userInfo["data"]!).rawString()!.convertToDictionary(), let objectId = params.value(forKey: "estimatedCost") as? Int{
            estimatedCost = objectId
        }
        if let params = JSON(userInfo["data"]!).rawString()!.convertToDictionary(), let objectId = params.value(forKey: "estimatedTime") as? Int{
            estimatedTime = objectId
        }
        
        if  let params = JSON(userInfo["data"]!).rawString()!.convertToDictionary(),
            let firstname =  params.value(forKey: "firstname") as? String ,
            let lastname =  params.value(forKey: "lastname") as? String{
            fullName = firstname + " " + lastname
        }
        
        if let params = JSON(userInfo["data"]!).rawString()!.convertToDictionary(), let objectId = params.value(forKey: "phoneNumber") as? String{
            phoneNumber = objectId
        }
        if let params = JSON(userInfo["data"]!).rawString()!.convertToDictionary(), let objectId = params.value(forKey: "pictureURL") as? String{
            pictureURL = objectId
        }
        if let params = JSON(userInfo["data"]!).rawString()!.convertToDictionary(), let objectId = params.value(forKey: "tripId") as? String{
            tripId = objectId
        }
        if let params = JSON(userInfo["data"]!).rawString()!.convertToDictionary(), let objectId = params.value(forKey: "sentAt") as? String{
            sentAt = objectId
        }

        let notification = NotificationStruct.init( type: type, pickUpAddress: pickUpAddress, pickUpLatitude: pickUpLatitude, pickUpLongitude: pickUpLongitude, destinationAddress: destinationAddress, destinationLatitude: destinationLatitude, destinationLongitude: destinationLongitude, initialDistance: initialDistance, estimatedTime: estimatedTime, estimatedCost: estimatedCost, fullName: fullName, pictureURL: pictureURL, phoneNumber: phoneNumber, tripId: tripId, sentAt: sentAt)
        
        
        return notification
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        guard let _ = UserDefaults.standard.value(forKey: "notifType10")else {
            if Auth.auth().currentUser != nil {
                print("applicationWillTerminate")
                Driver.connectDriver(status: 0)
            }
            return
        }
    }
    
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }
    
    // This function is added here only for debugging purposes, and can be removed if swizzling is enabled.
    // If swizzling is disabled then this function must be implemented so that the APNs token can be paired to
    // the FCM registration token.
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        print("APNs token retrieved: \(deviceTokenString)")
        // With swizzling disabled you must set the APNs token here.
        // Messaging.messaging().apnsToken = deviceToken
    }
    
    func showRaceNotification(json : JSON)
    {
        if let top = UIApplication.topViewController() {
            
            Driver.connectDriver(status: 2)
            
            if top.className == NotificationViewController.className{
                
                let vc = top as! NotificationViewController
                
                vc.notifArray = [json]
                
            }else{
                let stb = UIStoryboard.init(name: "Main", bundle: nil)
                
                let notifNvc = stb.instantiateViewController(withIdentifier: "notifNVC") as! UINavigationController
                
                let newsVc = notifNvc.topViewController as! NotificationViewController
                
                newsVc.notifArray = [json]
                
                top.present(notifNvc, animated: true, completion: nil)
            }
        }else{
            //showToast(NO_INTERNET_ERROR_STR, backgroundColor: toastColor, textColor: .white, font: h2Regular!)
        }
    }

}

// [START ios_10_message_handling]
@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        print(userInfo)
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        let state = UIApplication.shared.applicationState
        
         if state == .active {
            if self.getNotification(from: userInfo).type == 10 {
                let data = JSON.init(parseJSON: JSON(userInfo["data"]!).rawString()!)
                self.showRaceNotification(json: data)
            }
        }
        // Don't alert the user for other types.
        completionHandler(UNNotificationPresentationOptions(rawValue: 0))

        // Change this to your preferred presentation option
        //completionHandler([])
    }

    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        // Get the meeting ID from the original notification.
        let userInfo = response.notification.request.content.userInfo
        print(userInfo)
        let state = UIApplication.shared.applicationState

        if state == .background || state == .inactive {
            print("app in background")
            if self.getNotification(from: userInfo).type == 10 {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
                
                let date = Date()
                if let sentAt = dateFormatter.date(from: self.getNotification(from: userInfo).sentAt){
                    if 15 - Int(date.timeIntervalSince(sentAt)) > 0{
                        let data = JSON.init(parseJSON: JSON(userInfo["data"]!).rawString()!)
                        self.showRaceNotification(json: data)
                    }else {
                        let banner = NotificationBanner(title: "Trip Canceled", subtitle: "Customer has cancel the trip", style: .warning)
                        banner.show()
                    }
                }
            }
        }
        completionHandler()
    }
}
// [END ios_10_message_handling]

extension AppDelegate : MessagingDelegate {
    // [START refresh_token]
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        
        if let driver = Auth.auth().currentUser {
            
            Driver.updateDeviceToken(driver.uid, fcmToken)
            print("device token driver updated : ",fcmToken)
            
        }else {
            UserDefaults.standard.set(fcmToken, forKey: "RegistrationToken")
        }
        
        //let dataDict:[String: String] = ["token": fcmToken]
        //NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
    }
    // [END refresh_token]
    // [START ios_10_data_message]
    // Receive data messages on iOS 10+ directly from FCM (bypassing APNs) when the app is in the foreground.
    // To enable direct data messages, you can set Messaging.messaging().shouldEstablishDirectChannel to true.
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
    }
    // [END ios_10_data_message]
}
