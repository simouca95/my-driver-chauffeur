//
//  AlertView.swift
//  Exchange
//
//  Created by Lhioui Mahmoud on 6/22/17.
//  Copyright © 2017 Mahmoud. All rights reserved.
//
import UIKit
import STPopup

typealias AlertHandler = () -> Void

class AlertView: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var oneButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var submitButton: UIButton!
    
    var titleString: String!
    var isOneButton: Bool!
    var cancelButtonTitle: String!
    var submitButtonTitle: String!
    var cancelAction : AlertHandler?
    var doneAction : AlertHandler?
    
    init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?, isOneButton: Bool, title: String,cancelButtonTitle:String,submitButtonTitle:String, cancelHandler: AlertHandler? = nil, doneHandler:AlertHandler? = nil) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        self.isOneButton = isOneButton
        self.titleString = title
        self.cancelButtonTitle = cancelButtonTitle
        self.submitButtonTitle = submitButtonTitle
        self.cancelAction = cancelHandler
        self.doneAction = doneHandler
        let screenSize = UIScreen.main.bounds.size
        let popupWidth = screenSize.width - 80
        let textWidth = popupWidth - 40
        let stringRect = title.boundingRect(with: CGSize(width: textWidth, height: CGFloat.greatestFiniteMagnitude),options: [.usesFontLeading,.usesLineFragmentOrigin], attributes:[NSAttributedStringKey.font: UIFont.systemFont(ofSize: 17)], context:nil)
        let stringHeight = stringRect.integral.size.height
        let popupSize = CGSize(width: popupWidth, height: stringHeight+150)
        
        self.contentSizeInPopup = popupSize
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.popupController?.navigationBarHidden = true
        self.popupController?.containerView.layer.cornerRadius = 10
        self.popupController?.containerView.clipsToBounds = true
        self.popupController?.hidesCloseButton = true
        self.popupController?.navigationBarHidden = true

        oneButton.layer.cornerRadius = cancelButton.frame.height/2
        oneButton.clipsToBounds = true
        cancelButton.layer.cornerRadius = cancelButton.frame.height/2
        cancelButton.clipsToBounds = true
        submitButton.layer.cornerRadius = submitButton.frame.height/2
        submitButton.clipsToBounds = true
        
        titleLabel.text = self.titleString
        
        if isOneButton == true{
            self.submitButton.isHidden = true
            self.cancelButton.isHidden = true
        }else{
           self.oneButton.isHidden = true
        }
        self.oneButton.setTitle(self.cancelButtonTitle, for: .normal)
        self.cancelButton.setTitle(self.cancelButtonTitle, for: .normal)
        self.submitButton.setTitle(self.submitButtonTitle, for: .normal)
        popupController?.backgroundView?.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(self.dismissPopup)))
    }
    
    @objc func dismissPopup(){
        self.popupController?.dismiss()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func cancelAction(_ sender: UIButton) {
        self.popupController?.dismiss(completion: cancelAction)
    }
    
    @IBAction func submitAction(_ sender: UIButton) {
        self.popupController?.dismiss(completion: doneAction)
    }
    
}
