//
//  common.swift
//  Shoot
//
//  Created by macbook on 24/04/2018.
//  Copyright © 2018 Karizma. All rights reserved.
//

import Foundation
import UIKit
//import Toaster
/*
 
 STC
 == STC-Light
 == STC-Regular
 == STC-Bold
 == STC-Italic
 
 */

let h1Regular = UIFont(name:"STC-Regular" ,size:19)//DroidArabicKufi
let h2Regular = UIFont(name:"STC-Regular", size:17)//DroidArabicKufi
let h3Regular = UIFont(name:"STC-Regular", size:15)//DroidArabicKufi
let h4Regular = UIFont(name:"STC-Bold", size:11)//DroidArabicKufi
let h2RegularBold = UIFont(name:"STC-Bold", size:17)//DroidArabicKufi
let h3RegularBold = UIFont(name:"STC-Bold", size:14)
let successColor = UIColor(hex: 0x008040)
let errorColor = UIColor(hex: 0xFF0000)
let toastColor = UIColor(hex: 0xBD242B)


//func showToast(_ message: String, backgroundColor: UIColor, textColor: UIColor, font: UIFont) {
//    ToastCenter.default.cancelAll()
//    let appearance = ToastView.appearance()
//    appearance.backgroundColor = backgroundColor
//    appearance.textColor = textColor
//    appearance.font = font
//    Toast(text: message, duration: Delay.short).show()
//}

struct Country {
    var nameAr: String
    var nameEn: String
    var code: String
    var dialCode: String
}

func loadJsonRessource(from fileName: String)->[NSDictionary]{
    var jsonArray = [NSDictionary]()
    let path = Bundle.main.path(forResource: fileName, ofType: "json")
    do {
        let data = try Data(contentsOf: URL(fileURLWithPath: path!), options: .mappedIfSafe)
        let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [NSDictionary]
        if json != nil{
            jsonArray = json!
        }
    } catch {
        print(error)
    }
    return jsonArray
}

func loadJSONCountries()->NSMutableArray{
    let countryArray = NSMutableArray()
    let countries = loadJsonRessource(from: "Countries")
    for index in 0...countries.count-1{
        let obj = countries[index] as! [String : String]
        let country = Country(nameAr: obj["name_ar"]!, nameEn: obj["name_en"]!, code: obj["code"]!, dialCode: obj["dial_code"]!)
        countryArray.add(country)
    }
    return countryArray
}

func getMatchTime(_ time: String) -> String {
    
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "HH:mm:ss"
    
    dateFormatter.timeZone = TimeZone.current

    let date = dateFormatter.date(from: time)
    
    // To convert the date into an HH:mm format
    dateFormatter.dateFormat = "HH:mm"
    
    dateFormatter.locale = Locale.init(identifier: "En")
    
    if let date = date {
        
        return dateFormatter.string(from: date)
    }
    
    return ""
}

func presentVC(vc : String , self : UIViewController) {
    let stb = UIStoryboard(name: "Main", bundle: nil)
    let introMain = stb.instantiateViewController(withIdentifier: vc)
    introMain.modalPresentationStyle = .custom
    introMain.modalTransitionStyle = .crossDissolve
    self.present(introMain, animated: true, completion: nil)
}
func shakeView(shakeView: UIView) {
    
    let shake = CABasicAnimation(keyPath: "position")
    let xDelta = CGFloat(5)
    shake.duration = 0.15
    shake.repeatCount = 2
    shake.autoreverses = true
    
    let from_point = CGPoint.init(x: shakeView.center.x - xDelta, y: shakeView.center.y)
    let from_value = NSValue(cgPoint: from_point)
    
    let to_point = CGPoint.init(x: shakeView.center.x + xDelta, y: shakeView.center.y)
    let to_value = NSValue(cgPoint: to_point)
    
    shake.fromValue = from_value
    shake.toValue = to_value
    shake.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
    shakeView.layer.add(shake, forKey: "position")
}

//func getUserAvatar(_ avatar: Files, completionBlock: @escaping (String?)->Void) {
//
//    avatar.fetchIfNeededInBackground(block: { (object, error) in
//
//        if error == nil {
//
//            if let large = avatar.large {
//
//                completionBlock(large)
//            }
//            else if let medium = avatar.medium {
//
//                completionBlock(medium)
//            }
//            else if let small = avatar.small {
//
//                completionBlock(small)
//            }else{
//
//                completionBlock(nil)
//            }
//        }
//    })
//}

//func getImageFile(_ file: Files, completionBlock: @escaping (String?)->Void) {
//
//    file.fetchIfNeededInBackground(block: { (object, error) in
//
//        if error == nil {
//
//            if let large = file.large {
//
//                completionBlock(large)
//            }
//            else if let medium = file.medium {
//
//                completionBlock(medium)
//            }
//            else if let small = file.small {
//
//                completionBlock(small)
//            }else{
//
//                completionBlock(nil)
//            }
//        }
//    })
//}
//
//import Parse
//
//func getStrings(_ key: Language?)->String?{
//
//    if Languages.currentLanguage().starts(with: "ar") {
//
//        if let language = key {
//
//            if let ar = language.ar {
//
//                return ar
//            }
//
//            if let en = language.en {
//
//                return en
//
//            }
//        }
//    }else{
//
//        if let language = key {
//
//            if let en = language.en {
//
//                return en
//
//            }
//
//            if let ar = language.ar {
//
//                return ar
//            }
//        }
//    }
//
//
//    return nil
//}

/**
 struct contains notifications attributes
 */
struct NotificationStruct{
    var type: Int
    var pickUpAddress: String
    var pickUpLatitude: Double
    var pickUpLongitude: Double
    var destinationAddress: String
    var destinationLatitude: Double
    var destinationLongitude: Double
    var initialDistance: Int
    var estimatedTime: Int
    var estimatedCost: Int
    var fullName: String
    var pictureURL: String
    var phoneNumber: String
    var tripId : String
    var sentAt : String
}

func getLanguage()->String{
    
    if UIApplication.shared.userInterfaceLayoutDirection == .rightToLeft {
        
        return "ar"
    }
    return "en"
}

func getNoConnectionImage()->UIImage {
    
    if Languages.currentLanguage().starts(with: "ar") {
        
       return  #imageLiteral(resourceName: "internet_ar")
    }
    
    return #imageLiteral(resourceName: "no_connection")
}

func getNoWinnerImage()->UIImage {
    
    if Languages.currentLanguage().starts(with: "ar") {
        
        return  #imageLiteral(resourceName: "nowinnersAR")
    }
    
    return #imageLiteral(resourceName: "nowinners1")
}

func getNothingImage()->UIImage {
    
    if Languages.currentLanguage().starts(with: "ar") {
        
        return  #imageLiteral(resourceName: "nothing_ar")
    }
    
    return #imageLiteral(resourceName: "nothing")
}
