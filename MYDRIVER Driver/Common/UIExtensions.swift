//
//  UIExtensions.swift
//  Exchange
//
//  Created by Lhioui Mahmoud on 6/22/17.
//  Copyright © 2017 Mahmoud. All rights reserved.
//

import Foundation
import UIKit

private var flatAssociatedObjectKey: UInt8 = 0
private var palceHolderColorAssociatedObject: UInt8 = 0
var vSpinner : UIView?

extension UITextField {
    @IBInspectable var placeHolderTextColor :UIColor{
        get {
            guard let obj = objc_getAssociatedObject(self, &palceHolderColorAssociatedObject) as? UIColor else {
                return UIColor.gray
            }
            return obj
        }
        set{
            if let palceHoloder = placeholder {
                attributedPlaceholder = NSAttributedString(string: palceHoloder, attributes: [NSAttributedStringKey.foregroundColor: newValue])
            }else{
                attributedPlaceholder = NSAttributedString(string: "Default", attributes: [NSAttributedStringKey.foregroundColor: newValue])
            }
            objc_setAssociatedObject(self, &palceHolderColorAssociatedObject, newValue,
                                     objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    @IBInspectable var alignment:Bool {
        set {
            if newValue {
                
                if UIApplication.shared.userInterfaceLayoutDirection == .rightToLeft {
                    
                    self.textAlignment = .right
                }
            }
        }
        
        get{
            
            return false
        }
    }
    
}
extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    func colorByDarkeningColorWithValue(value:CGFloat)->UIColor{
        
        let totalComponents = self.cgColor.numberOfComponents
        let isGreyscale = (totalComponents == 2) ? true : false
        
        let oldComponents = self.cgColor.components
        var newComponents = [CGFloat]()
        
        if (isGreyscale) {
            newComponents.append((oldComponents?[0])! - value < 0.0 ? 0.0 : (oldComponents?[0])! - value)
            newComponents.append((oldComponents?[0])! - value < 0.0 ? 0.0 : (oldComponents?[0])! - value)
            newComponents.append((oldComponents?[0])! - value < 0.0 ? 0.0 : (oldComponents?[0])! - value)
            newComponents.append((oldComponents?[1])!)
        }else {
            newComponents.append((oldComponents?[0])! - value < 0.0 ? 0.0 : (oldComponents?[0])! - value)
            newComponents.append((oldComponents?[1])! - value < 0.0 ? 0.0 : (oldComponents?[1])! - value)
            newComponents.append((oldComponents?[2])! - value < 0.0 ? 0.0 : (oldComponents?[2])! - value)
            newComponents.append((oldComponents?[3])!)
        }
        
        let colorSpace = CGColorSpaceCreateDeviceRGB();
        let newColor = CGColor(colorSpace: colorSpace, components: newComponents);
        let retColor = UIColor(cgColor: newColor!)
        
        return retColor;
    }
    convenience init(hex: Int) {
        self.init(hex: hex, a: 1.0)
    }
    
    convenience init(hex: Int, a: CGFloat) {
        self.init(r: (hex >> 16) & 0xff, g: (hex >> 8) & 0xff, b: hex & 0xff, a: a)
    }
    
    convenience init(r: Int, g: Int, b: Int) {
        self.init(r: r, g: g, b: b, a: 1.0)
    }
    
    convenience init(r: Int, g: Int, b: Int, a: CGFloat) {
        self.init(red: CGFloat(r) / 255.0, green: CGFloat(g) / 255.0, blue: CGFloat(b) / 255.0, alpha: a)
    }
    
    convenience init?(hexString: String) {
        guard let hex = hexString.hex else {
            return nil
        }
        self.init(hex: hex)
    }
    
    func hexStringFromColor() -> String {
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0
        
        getRed(&r, green: &g, blue: &b, alpha: &a)
        
        let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0
        
        return String(format:"%06x", rgb)
    }    
}


/*
 An extension that adds a "flat" field to UINavigationBar. This flag, when
 enabled, removes the shadow under the navigation bar.
 */
@IBDesignable extension UINavigationBar {
    @IBInspectable var flat: Bool {
        get {
            guard let obj = objc_getAssociatedObject(self, &flatAssociatedObjectKey) as? NSNumber else {
                return false
            }
            return obj.boolValue;
        }
        
        set {
            if (newValue) {
                let void = UIImage()
                setBackgroundImage(void, for: .any, barMetrics: .default)
                shadowImage = void
            } else {
                setBackgroundImage(nil, for: .any, barMetrics: .default)
                shadowImage = nil
            }
            objc_setAssociatedObject(self, &flatAssociatedObjectKey, NSNumber(value: newValue),
                                     objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
}
extension UIButton {
    
    @IBInspectable var localizedInset :CGFloat{
        get {
            return 10
        }
        set{
            
            if Languages.currentLanguage().starts(with: "ar") {
                
                self.imageEdgeInsets.left = localizedInset
            }
            else {
                
                self.imageEdgeInsets.right = localizedInset
            }
        }
    }
    
    func centerButtonAndImageWithSpacing(spacing :CGFloat){
        self.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, spacing);
        self.titleEdgeInsets = UIEdgeInsetsMake(0, spacing, 0, 0);
    }
    
    func flipImageAndLabel(){
        self.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        if(self.titleLabel != nil && self.imageView != nil){
            self.titleLabel!.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
            self.imageView!.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        }
    }
    func alignImageRight() {
        if let titleLabel = self.titleLabel, let imageView = self.imageView {
            // Force the label and image to resize.
            titleLabel.sizeToFit()
            imageView.sizeToFit()
            imageView.contentMode = .scaleAspectFit
            
            // Set the insets so that the title appears to the left and the image appears to the right.
            // Make the image appear slightly off the top/bottom edges of the button.
            self.titleEdgeInsets = UIEdgeInsets(top: 0, left: -1 * (imageView.frame.size.width + 4),
                                                bottom: 0, right: imageView.frame.size.width + 4)
            self.imageEdgeInsets = UIEdgeInsets(top: 4, left: titleLabel.frame.size.width + 4,
                                                bottom: 4, right: -1 * (titleLabel.frame.size.width + 4))
        }
    }
    func setUpAsTag(title:String,image:UIImage,spacing:CGFloat,bgColor:UIColor,textFont:UIFont,textColor:UIColor){
        self.setTitle(title, for: .normal)
        self.setImage(image, for: .normal)
        self.backgroundColor = bgColor
        self.layer.cornerRadius = 5
        self.setTitleColor(textColor, for: .normal)
        self.titleLabel?.font = textFont
        alignImageRight()
    }
    func bounce() {
        self.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.animate(withDuration: 2.0,
                       delay: 0,
                       usingSpringWithDamping: 0.2,
                       initialSpringVelocity: 6.0,
                       options: .allowUserInteraction,
                       animations: { [weak self] in
                        self?.transform = .identity
                        },completion: nil)
    }
}
extension UIImage{
    
    func rotate(radians: Float) -> UIImage? {
        var newSize = CGRect(origin: CGPoint.zero, size: self.size).applying(CGAffineTransform(rotationAngle: CGFloat(radians))).size
        // Trim off the extremely small float value to prevent core graphics from rounding it up
        newSize.width = floor(newSize.width)
        newSize.height = floor(newSize.height)
        
        UIGraphicsBeginImageContextWithOptions(newSize, true, self.scale)
        let context = UIGraphicsGetCurrentContext()!
        
        // Move origin to middle
        context.translateBy(x: newSize.width/2, y: newSize.height/2)
        // Rotate around middle
        context.rotate(by: CGFloat(radians))
        
        self.draw(in: CGRect(x: -self.size.width/2, y: -self.size.height/2, width: self.size.width, height: self.size.height))
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
    func resizedCroppedImage(newSize:CGSize) -> UIImage {
        var ratio: CGFloat = 0
        var delta: CGFloat = 0
        var offset = CGPoint(x: 0, y: 0)
        if self.size.width > self.size.height {
            ratio = newSize.width / self.size.width
            delta = (ratio * self.size.width) - (ratio * self.size.height)
            offset = CGPoint(x:delta / 2, y:0)
        } else {
            ratio = newSize.width / self.size.height
            delta = (ratio * self.size.height) - (ratio * self.size.width)
            offset = CGPoint(x:0, y:delta / 2)
        }
        let clipRect = CGRect(x:-offset.x, y:-offset.y, width:(ratio * self.size.width) + delta, height:(ratio * self.size.height) + delta)
        UIGraphicsBeginImageContextWithOptions(newSize, true, 0.0)
        UIRectClip(clipRect)
        self.draw(in: clipRect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
    
    var rounded: UIImage? {
        let imageView = UIImageView(image: self)
        imageView.layer.cornerRadius = min(size.height/2, size.width/2)
        imageView.layer.masksToBounds = true
        UIGraphicsBeginImageContext(imageView.bounds.size)
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        imageView.layer.render(in: context)
        let result = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return result
    }
    var circle: UIImage? {
        let square = CGSize(width: min(size.width, size.height), height: min(size.width, size.height))
        let imageView = UIImageView(frame: CGRect(origin: CGPoint(x: 0, y: 0), size: square))
        imageView.contentMode = .scaleAspectFill
        imageView.image = self
        imageView.layer.cornerRadius = square.width/2
        imageView.layer.masksToBounds = true
        UIGraphicsBeginImageContext(imageView.bounds.size)
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        imageView.layer.render(in: context)
        let result = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return result
    }
    
    func imageWithColor(color: UIColor, size: CGSize) -> UIImage {
        
        let rect = CGRect(x:0, y:0, width:size.width, height:size.height)
        
        UIGraphicsBeginImageContext(rect.size)
        
        let context = UIGraphicsGetCurrentContext()
        context!.setFillColor(color.cgColor);
        context!.fill(rect);
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext();
        
        return image!;
    }
    
}

extension UINavigationController {
    func hideNavigationBar(hide:Bool){
        self.setNavigationBarHidden(hide, animated: false)
    }
    func setNavigationControllerTransparent(){
        self.navigationBar.backgroundColor = UIColor.clear
        self.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationBar.shadowImage = UIImage()
    }
}

extension UIBarButtonItem {
    
    @IBInspectable var backButton:Bool {
        set {
            if newValue {
                
                if Languages.currentLanguage().starts(with: "ar") {
                    
                    self.image = #imageLiteral(resourceName: "back_ar")
                }
            }
        }
        
        get{
            
            return false
        }
    }
    
//    @IBInspectable var notifBadge:Bool {
//        set {
//            if newValue {
//                
//                if UserDefaults.standard.integer(forKey: "notifs_count") > 0 {
//                    
//                    self.badgeValue = "\(UserDefaults.standard.integer(forKey: "notifs_count"))"
//                    
//                    self.badgeBGColor = toastColor
//                    
//                    self.badgeTextColor = .white
//                }
//            }
//        }
//        
//        get{
//            
//            return false
//        }
//    }
}

extension UIView {
    
    @IBInspectable var backButton:Bool {
        set {
            if newValue {
            
                if Languages.currentLanguage().starts(with: "ar") {
                
                    self.transform = self.transform.rotated(by: CGFloat(Float.pi))
                }
            }
        }
        
        get{
            
            return false
        }
    }
    
    func alphaGradientY(_ reverse: Bool) {
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 100)
        if !reverse {
            gradientLayer.startPoint = CGPoint(x:0.5,y:0.0)
            gradientLayer.endPoint = CGPoint(x:0.5,y:1.0)
            gradientLayer.locations = [(0.0), (0.2), (0.4), (0.6), (0.8) , (1.0)]


        }else{
            gradientLayer.startPoint = CGPoint(x:0.5,y:1.0)
            gradientLayer.endPoint = CGPoint(x:0.5,y:0.0)
            gradientLayer.locations = [(0.0), (0.2), (0.4), (0.6), (0.8) , (1.0)].reversed()

        }
        
        gradientLayer.colors = [UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha:            1).cgColor,
                                UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.8).cgColor,
                                UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.6).cgColor,
                                UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.4).cgColor,
                                UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.2).cgColor,
                                UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 0).cgColor]
        self.layer.addSublayer(gradientLayer)
    }
    
    func alphaGradient(_ reverse: Bool) {
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.bounds;
        if !reverse {
            gradientLayer.startPoint = CGPoint(x:0.0,y:0.5)
            gradientLayer.endPoint = CGPoint(x:1.0,y:0.5)
        }else{
            gradientLayer.startPoint = CGPoint(x:1.0,y:0.5)
            gradientLayer.endPoint = CGPoint(x:0.0,y:0.5)
        }
        gradientLayer.locations = [0.0, 0.35]
        gradientLayer.colors = [UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.9).cgColor,
                                UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.3).cgColor,
                                UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.0).cgColor]
        self.layer.addSublayer(gradientLayer)
    }
    
    func shake(){
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.05
        animation.repeatCount = 5
        animation.autoreverses = true
        animation.fromValue = NSValue(cgPoint: CGPoint(x: self.center.x - 2.0,y: self.center.y))
        animation.toValue = NSValue(cgPoint: CGPoint(x: self.center.x + 2.0,y: self.center.y))
        self.layer.add(animation, forKey: "position")
    }
    
    func setupContentViewForViewWithScroll(contentView vwContent : UIView) {
        //Set constraint for scrollview content
        let constraint = NSLayoutConstraint(item: vwContent, attribute: NSLayoutAttribute.height, relatedBy: .equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: self.bounds.size.height)
        vwContent.addConstraint(constraint)
        self.layoutSubviews()
    }
    
    func startShummering(){
        let light = UIColor(white: 0, alpha: 0.4).cgColor
        let dark = UIColor.black.cgColor
        let layerGradient = CAGradientLayer(layer: layer)
        layerGradient.colors=[dark,light,dark]
        layerGradient.frame = CGRect(x:-self.bounds.size.width, y:0, width:3*self.bounds.size.width, height:self.bounds.size.height)
        layerGradient.startPoint = CGPoint(x:0.0, y:0.5)
        layerGradient.endPoint   = CGPoint(x:1.0, y:0.525)
        layerGradient.locations  = [0.4, 0.5, 0.6]
        self.layer.mask = layerGradient
        let animation = CABasicAnimation(keyPath: "locations")
        animation.fromValue = [0.0, 0.1, 0.2]
        animation.toValue = [0.8, 0.9, 1.0]
        animation.duration = 1.5;
        animation.repeatCount = Float.infinity
        layerGradient.add(animation, forKey: "shimmer")
    }
    
    func stopShmmering(){
        self.layer.mask = nil
    }
    
    func insertBlurView (style: UIBlurEffectStyle) {
        self.backgroundColor = UIColor.clear
        let blurEffect = UIBlurEffect(style: style)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.bounds
        self.insertSubview(blurEffectView, at: 0)
    }
    
    var snapshot: UIImage {
        UIGraphicsBeginImageContextWithOptions(bounds.size, false, UIScreen.main.scale)
        drawHierarchy(in: bounds, afterScreenUpdates: true)
        let result = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return result!
    }
    
    func snapshot(with frame: CGRect)->UIImage{
        UIGraphicsBeginImageContextWithOptions(frame.size, false, UIScreen.main.scale)
        drawHierarchy(in: frame, afterScreenUpdates: true)
        let result = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return result!
    }
    
    func shadowWithCornder(raduis: CGFloat, shadowColor: CGColor, shadowOffset: CGSize, shadowOpacity: Float, shadowRadius: CGFloat){
        let layer = self.layer
        layer.masksToBounds = false
        layer.cornerRadius = raduis
        layer.shadowColor = shadowColor
        layer.shadowOffset = shadowOffset
        layer.shadowOpacity = shadowOpacity
        layer.shadowRadius = shadowRadius
    }
    
    func borderWithCornder(raduis: CGFloat, borderColor: CGColor, borderWidth: CGFloat){
        layer.cornerRadius = raduis
        clipsToBounds = true
        layer.borderColor = borderColor
        layer.borderWidth = borderWidth
    }
    
    func roundedCorner(corner: UIRectCorner, raduis: CGFloat){
        let rectShape = CAShapeLayer()
        rectShape.bounds = frame
        rectShape.position = center
        rectShape.path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corner, cornerRadii: CGSize(width: raduis, height: raduis)).cgPath
        layer.mask = rectShape
    }
    
    func setCardView(cornerRadius: CGFloat, borderColor: CGColor, borderWidth: CGFloat, shadowOpacity: Float, shadowColor: CGColor, shadowRadius: CGFloat, shadowOffset: CGSize){
        self.layer.cornerRadius = cornerRadius
        self.layer.borderColor  =  borderColor
        self.layer.borderWidth = borderWidth
        self.layer.shadowOpacity = shadowOpacity
        self.layer.shadowColor =  shadowColor
        self.layer.shadowRadius = shadowRadius
        self.layer.shadowOffset = shadowOffset
        self.layer.masksToBounds = true
    }
    
}
extension UILabel {
    @IBInspectable var adjustFontToRealIPhoneSize:Bool {
        set {
            if newValue {
                let currentFont = self.font
                var sizeScale: CGFloat = 1
                let model = UIDevice.current.modelName
                
                if model == "iPhone 6" {
                    sizeScale = 1.1
                }
                else if model == "iPhone 6 Plus" {
                    sizeScale = 1.3
                }
                
                self.font = currentFont?.withSize((currentFont?.pointSize)! * sizeScale)
            }
        }
        
        get {
            return false
        }
    }
    
    @IBInspectable var alignment:Bool {
        set {
            if newValue {
                
                if UIApplication.shared.userInterfaceLayoutDirection == .rightToLeft {
                    
                    self.textAlignment = .right
                }
            }
        }
        
        get{
            
            return false
        }
    }
}
public extension UIDevice {
    
    var modelName: String {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        switch identifier {
        case "iPod5,1":                                 return "iPod Touch 5"
        case "iPod7,1":                                 return "iPod Touch 6"
        case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
        case "iPhone4,1":                               return "iPhone 4s"
        case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
        case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
        case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
        case "iPhone7,2":                               return "iPhone 6"
        case "iPhone7,1":                               return "iPhone 6 Plus"
        case "iPhone8,1":                               return "iPhone 6s"
        case "iPhone8,2":                               return "iPhone 6s Plus"
        case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
        case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad 3"
        case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad 4"
        case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
        case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
        case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad Mini"
        case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad Mini 2"
        case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad Mini 3"
        case "iPad5,1", "iPad5,2":                      return "iPad Mini 4"
        case "iPad6,7", "iPad6,8":                      return "iPad Pro"
        case "AppleTV5,3":                              return "Apple TV"
        case "i386", "x86_64":                          return "Simulator"
        default:                                        return identifier
        }
    }
    
}
extension String {
    
    //To check text field or String is blank or not
    var isBlank: Bool {
        get {
            let trimmed = trimmingCharacters(in: NSCharacterSet.whitespaces)
            return trimmed.isEmpty
        }
    }
    
    //Validate Email
    var isEmail: Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: self)
    }
    
    //validate PhoneNumber
    var isPhoneNumber: Bool {
        let charcter  = NSCharacterSet(charactersIn: "+0123456789٠١٢٣٤٥٦٧٨٩").inverted
        var filtered:String!
        let inputString:NSArray = self.components(separatedBy: charcter) as NSArray
        filtered = inputString.componentsJoined(by: "") as String
        return  self == filtered
        
    }
    
    func convertToEnglishNumbers() -> NSNumber? {
        let Formatter: NumberFormatter = NumberFormatter()
        Formatter.locale = Locale(identifier: "EN")
        if let final = Formatter.number(from: self) {
            return final
        }
        return nil
    }
    
    func trim()->String{
        return self.trimmingCharacters(in: NSCharacterSet.whitespaces)
    }
    
    func DateTimeAgoLocalizedStrings(key: String) -> String {
        return NSLocalizedString(key, tableName: "TimeAgoLocalization", bundle: Bundle.main, comment: "")
    }
    
    var hex: Int? {
        return Int(self, radix: 16)
    }
    
    func emojiFlag() -> String {
        var string = ""
        var country = self.uppercased()
        for uS in country.unicodeScalars {
            string.append("\(UnicodeScalar(127397 + uS.value)!)")
        }
        return string
    }
    
    func convertToDictionary() -> NSDictionary?{
        if let data = self.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    func stringByAddingPercentEncodingForURLQueryValue() -> String? {
        let allowedCharacters = NSCharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._~")
        return self.addingPercentEncoding(withAllowedCharacters: allowedCharacters as CharacterSet)
    }
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: font], context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func width(withConstraintedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: font], context: nil)
        
        return ceil(boundingBox.width)
    }
    
    func widthOfString(usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [NSAttributedStringKey.font: font]
        let size = self.size(withAttributes: fontAttributes)
        return size.width
    }
    
    func capitalizingFirstLetter() -> String {
        return prefix(1).uppercased() + dropFirst()
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
    
    func getDateWith(_ format: String)->Date?{
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = format
        
        if let date = dateFormatter.date(from: self) {
            
            return date
        }
        
        return nil
    }
    
    func isValidURL() -> Bool {
        
        if let url = URL(string: self) {
            
            return UIApplication.shared.canOpenURL(url)
        }
        
        return false
    }
}


extension Date {
    func isGreaterThanDate(dateToCompare: Date) -> Bool {
        //Declare Variables
        var isGreater = false
        
        //Compare Values
        if self.compare(dateToCompare) == ComparisonResult.orderedDescending {
            isGreater = true
        }
        
        //Return Result
        return isGreater
    }
    
    func isLessThanDate(dateToCompare: Date) -> Bool {
        //Declare Variables
        var isLess = false
        
        //Compare Values
        if self.compare(dateToCompare) == ComparisonResult.orderedAscending {
            isLess = true
        }
        
        //Return Result
        return isLess
    }
    
    func equalToDate(dateToCompare: Date) -> Bool {
        //Declare Variables
        var isEqualTo = false
        
        //Compare Values
        if self.compare(dateToCompare) == ComparisonResult.orderedSame {
            isEqualTo = true
        }
        
        //Return Result
        return isEqualTo
    }
    
    func addDays(daysToAdd: Int) -> Date {
        let secondsInDays: TimeInterval = Double(daysToAdd) * 60 * 60 * 24
        let dateWithDaysAdded: Date = self.addingTimeInterval(secondsInDays)
        
        //Return Result
        return dateWithDaysAdded
    }
    
    func addHours(hoursToAdd: Int) -> Date {
        let secondsInHours: TimeInterval = Double(hoursToAdd) * 60 * 60
        let dateWithHoursAdded: Date = self.addingTimeInterval(secondsInHours)
        
        //Return Result
        return dateWithHoursAdded
    }
    
    func addSeconds(secondsToAdd: Int) -> Date {
        let secondsInHours: TimeInterval = Double(secondsToAdd)
        let dateWithHoursAdded: Date = self.addingTimeInterval(secondsInHours)
        
        //Return Result
        return dateWithHoursAdded
    }
    
    /// Returns the amount of years from another date
    func years(from date: Date) -> Int {
        return Calendar.current.dateComponents([.year], from: date, to: self).year ?? 0
    }
    /// Returns the amount of months from another date
    func months(from date: Date) -> Int {
        return Calendar.current.dateComponents([.month], from: date, to: self).month ?? 0
    }
    /// Returns the amount of weeks from another date
    func weeks(from date: Date) -> Int {
        return Calendar.current.dateComponents([.weekOfYear], from: date, to: self).weekOfYear ?? 0
    }
    /// Returns the amount of days from another date
    func days(from date: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: date, to: self).day ?? 0
    }
    /// Returns the amount of hours from another date
    func hours(from date: Date) -> Int {
        return Calendar.current.dateComponents([.hour], from: date, to: self).hour ?? 0
    }
    /// Returns the amount of minutes from another date
    func minutes(from date: Date) -> Int {
        return Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0
    }
    /// Returns the amount of seconds from another date
    func seconds(from date: Date) -> Int {
        return Calendar.current.dateComponents([.second], from: date, to: self).second ?? 0
    }
    var yesterday: Date {
        return Calendar.current.date(byAdding: .day, value: -1, to: noon)!
    }
    var tomorrow: Date {
        return Calendar.current.date(byAdding: .day, value: 1, to: noon)!
    }
    var noon: Date {
        return Calendar.current.date(bySettingHour: 12, minute: 0, second: 0, of: self)!
    }
    var month: Int {
        return Calendar.current.component(.month,  from: self)
    }
    var year: Int {
        return Calendar.current.component(.year,  from: self)
    }
    var day: Int {
        return Calendar.current.component(.day,  from: self)
    }
    var isLastDayOfMonth: Bool {
        return tomorrow.month != month
    }
    public var remainingTime: String {
        let components = self.remainingDateComponents()
        
        if components.year! > 0 {
            
            if components.year! < 2 {
                if components.year! < 1 {
                    return "أقل من سنة"
                }else if components.year == 1{
                    return "سنة واحدة"
                }
                else{
                    return "أكثر من سنة"
                }
                
            }else if components.year == 2{
                return  "سنتين"
            }
            else if components.year! < 11{
                return stringFromFormat(format: "%%d سنوات", withValue: components.year!)
            }else{
                return stringFromFormat(format: "%%d سنة", withValue: components.year!)
            }
        }
        
        if components.month! > 0 {
            if components.month! < 2 {
                if components.month! < 1{
                    return "أقل من شهر"
                }else if components.month! == 1{
                    return "شهر"
                }else{
                    return "أكثر من الشهر"
                }
                
            }else if components.month! == 2 {
                return "شهرين"
            }
            else if components.month! < 11 {
                return stringFromFormat(format: "%%d أشهر", withValue: components.month!)
            }else{
                return stringFromFormat(format: "%%d شهر", withValue: components.month!)
            }
        }
        
        if components.day! >= 7 {
            let week = components.day!/7
            if week < 2 {
                if week < 1{
                    return "أقل من أسبوع"
                }else if week == 1{
                    return "أسبوع"
                }else{
                    return "أكثر من أسبوع"
                }
                
            }else if week == 2 {
                return "أسبوعين"
            }
            else {
                return stringFromFormat(format: "%%d أسابيع", withValue: week)
            }
        }
        
        if components.day! > 0 {
            if components.day! < 2 {
                if components.day! < 1{
                    return "أقل من يوم"
                }else if components.day! == 1 {
                    return "يوم"
                }else{
                    return "أكثر من يوم"
                }
            }else if components.day! == 2 {
                return "يومين"
            }
            else if components.day! < 11 {
                return stringFromFormat(format: "%%d أيام", withValue: components.day!)
            }else{
                return stringFromFormat(format: "%%d يوم", withValue: components.day!)
            }
        }
        
        if components.hour! > 0 {
            if components.hour! < 2 {
                return "ساعة"
            }else if components.day! == 2 {
                return "ساعتين"
            }
            else if components.hour! < 11 {
                return stringFromFormat(format: "%%d ساعات", withValue: components.hour!)
            }else{
                return stringFromFormat(format: "%%d ساعة", withValue: components.hour!)
            }
        }
        
        if components.minute! > 0 {
            if components.minute! < 2 {
                return "دقيقة"
            }else if components.day! == 2 {
                return "دقيقتين"
            }
            else if components.minute! < 11{
                return stringFromFormat(format: "%%d دقائق", withValue: components.minute!)
            }else{
                return stringFromFormat(format: "%%d دقيقة", withValue: components.minute!)
            }
        }
        
        if components.second! > 0 {
            if components.second! < 5 {
                return "الآن"
            } else if components.second! < 11 {
                return stringFromFormat(format: "%%d ثوانٍ", withValue: components.second!)
            }else{
                return stringFromFormat(format: "%%d ثانية", withValue: components.second!)
            }
        }
        
        return ""
    }
    
    public var timeArAgo: String {
        let components = self.dateComponents()
        
        if components.year! > 0 {
            
            if components.year! < 2 {
                if components.year! < 1 {
                    return "هذا العام"
                }else if components.year == 1{
                    return "منذ عام"
                }
                else{
                    return "العام الماضي"
                }
                
            }else if components.year == 2{
                return "منذ عامين"
            }
            else if components.year! < 11{
                return stringFromFormat(format: "منذ %%d سنوات", withValue: components.year!)
            }else{
                return stringFromFormat(format: "منذ %%d سنة", withValue: components.year!)
            }
        }
        
        if components.month! > 0 {
            if components.month! < 2 {
                if components.month! < 1{
                    return "هذا الشهر"
                }else if components.month! == 1{
                    return "منذ شهر"
                }else{
                    return "الشهر الماضي"
                }
                
            }else if components.month! == 2 {
                return "منذ شهرين"
            }
            else if components.month! < 11 {
                return stringFromFormat(format: "منذ %%d أشهر", withValue: components.month!)
            }else{
                return stringFromFormat(format: "منذ %%d شهر", withValue: components.month!)
            }
        }
        
        if components.day! >= 7 {
            let week = components.day!/7
            if week < 2 {
                if week < 1{
                    return "هذا الأسبوع"
                }else if week == 1{
                    return "منذ أسبوع"
                }else{
                    return "الأسبوع الماضي"
                }
                
            }else if week == 2 {
                return "منذ أسبوعين"
            }
            else {
                return stringFromFormat(format: "منذ %%d أسابيع", withValue: week)
            }
        }
        
        if components.day! > 0 {
            if components.day! < 2 {
                if components.day! < 1{
                    return "اليوم"
                }else if components.day! == 1 {
                    return "منذ يوم"
                }else{
                    return "أمس"
                }
            }else if components.day! == 2 {
                return "منذ يومين"
            }
            else if components.day! < 11 {
                return stringFromFormat(format: "منذ %%d أيام", withValue: components.day!)
            }else{
                return stringFromFormat(format: "منذ %%d يوم", withValue: components.day!)
            }
        }
        
        if components.hour! > 0 {
            if components.hour! < 2 {
                return "منذ ساعة"
            }else if components.day! == 2 {
                return "منذ ساعتين"
            }
            else if components.hour! < 11 {
                return stringFromFormat(format: "منذ %%d ساعات", withValue: components.hour!)
            }else{
                return stringFromFormat(format: "منذ %%d ساعة", withValue: components.hour!)
            }
        }
        
        if components.minute! > 0 {
            if components.minute! < 2 {
                return "منذ دقيقة"
            }else if components.day! == 2 {
                return "منذ دقيقتين"
            }
            else if components.minute! < 11{
                return stringFromFormat(format: "منذ %%d دقائق", withValue: components.minute!)
            }else{
                return stringFromFormat(format: "منذ %%d دقيقة", withValue: components.minute!)
            }
        }
        
        if components.second! > 0 {
            if components.second! < 5 {
                return "حالًا"
            } else if components.second! < 11 {
                return stringFromFormat(format: "منذ %%d ثوانٍ", withValue: components.second!)
            }else{
                return stringFromFormat(format: "منذ %%d ثانية", withValue: components.second!)
            }
        }
        
        return ""
    }
    
    private func dateComponents() -> DateComponents {
        let calander = NSCalendar.current
        return calander.dateComponents([.second, .minute, .hour, .day, .month, .year], from: self, to: Date())
    }
    
    private func remainingDateComponents() -> DateComponents {
        let calander = NSCalendar.current
        return calander.dateComponents([.second, .minute, .hour, .day, .month, .year], from: Date(), to: self)
    }

    
    
    private func stringFromFormat(format: String, withValue value: Int) -> String {
        let localeFormat = String(format: format, getLocaleFormatUnderscoresWithValue(value: Double(value)))
        return String(format: localeFormat, value)
    }
    
    private func getLocaleFormatUnderscoresWithValue(value: Double) -> String {
        guard let localeCode = NSLocale.preferredLanguages.first else {
            return ""
        }
        // Russian (ru) and Ukrainian (uk)
        if localeCode == "ru" || localeCode == "uk" {
            let XY = Int(floor(value)) % 100
            let Y = Int(floor(value)) % 10
            
            if Y == 0 || Y > 4 || (XY > 10 && XY < 15) {
                return ""
            }
            
            if Y > 1 && Y < 5 && (XY < 10 || XY > 20) {
                return "_"
            }
            
            if Y == 1 && XY != 11 {
                return "__"
            }
        }
        
        return ""
    }
    
    func getFormattedDate(_ format: String) -> Date? {
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = format
        
        if let date = dateFormatter.date(from: dateFormatter.string(from: self)) {
            
            return date
        }
        
        return nil
    }
}

extension Dictionary {
    func stringFromHttpParameters() -> String {
        let parameterArray = self.map { (key, value) -> String in
            let percentEscapedKey = (key as! String).stringByAddingPercentEncodingForURLQueryValue()!
            let percentEscapedValue = (value as! String).stringByAddingPercentEncodingForURLQueryValue()!
            return "\(percentEscapedKey)=\(percentEscapedValue)"
        }
        
        return parameterArray.joined(separator: "&")
    }
    
}
extension NSNull {
    func length() -> Int { return 0 }
    func integerValue() -> Int { return 0 }
    func floatValue() -> Float { return 0 }
    func componentsSeparatedByString(separator: String) -> [AnyObject] { return [AnyObject]() }
    func objectForKey(key: AnyObject) -> AnyObject? { return nil }
    func boolValue() -> Bool { return false }
}
import STPopup
extension UIViewController{
    
    func showTwoChoicesActionSheet(_ title: String?, message: String?,_ firstChoiceTitle: String?, _ firstChoiceHandler: ((UIAlertAction)->Void)?,_ secondChoiceTitle: String?, _ secondChoiceHandler: ((UIAlertAction)->Void)?,_ cancelChoiceTitle: String?, _ cancelChoiceHandler: ((UIAlertAction)->Void)?){
        let alertController = UIAlertController.init(title: title, message: message, preferredStyle: .actionSheet)
        
        let firstChoice = UIAlertAction.init(title: firstChoiceTitle, style: .default, handler: firstChoiceHandler)
        alertController.addAction(firstChoice)
        
        let secondChoice = UIAlertAction.init(title: secondChoiceTitle, style: .default, handler: secondChoiceHandler)
        alertController.addAction(secondChoice)
        
        let cancelChoice = UIAlertAction.init(title: cancelChoiceTitle, style: .cancel, handler: cancelChoiceHandler)
        alertController.addAction(cancelChoice)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func showAlertView(isOneButton: Bool, title: String, cancelButtonTitle: String, submitButtonTitle: String, cancelHandler: AlertHandler? = nil, doneHandler:AlertHandler? = nil){
        let alertView = AlertView(nibName: "AlertView", bundle: nil, isOneButton: isOneButton, title: title, cancelButtonTitle: cancelButtonTitle, submitButtonTitle: submitButtonTitle, cancelHandler: cancelHandler, doneHandler:doneHandler)
        let alertPopup = STPopupController(rootViewController: alertView)
        alertPopup.present(in: self)
    }

//    func showAlertField(placeholder: String, submitButtonTitle: String, doneHandler:AlertHandler? = nil, chat: Chat, controller: MainChatViewController){
//        let alertView = AlertField(nibName: "AlertField", bundle: nil, fieldPlaceholder: placeholder, submitButtonTitle: submitButtonTitle, doneHandler: doneHandler, chat: chat, controller: controller)
//        let alertPopup = STPopupController(rootViewController: alertView)
//        alertPopup.present(in: self)
//    }

    func showAlertViewIn(viewController: UIViewController, isOneButton: Bool, title: String, cancelButtonTitle: String, submitButtonTitle: String, cancelHandler: AlertHandler? = nil, doneHandler:AlertHandler? = nil){
        let alertView = AlertView(nibName: "AlertView", bundle: nil, isOneButton: isOneButton, title: title, cancelButtonTitle: cancelButtonTitle, submitButtonTitle: submitButtonTitle, cancelHandler: cancelHandler, doneHandler:doneHandler)
        let alertPopup = STPopupController(rootViewController: alertView)
        alertPopup.present(in: viewController)
    }
    
    var appDelegate: AppDelegate{
        get{
            return UIApplication.shared.delegate as! AppDelegate
        }
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        tap.cancelsTouchesInView = false
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func showSpinner(onView : UIView) {
        let spinnerView = UIView.init(frame: onView.bounds)
        spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        let ai = UIActivityIndicatorView.init(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
        
        ai.startAnimating()
        ai.center = spinnerView.center
        
        DispatchQueue.main.async {
            spinnerView.addSubview(ai)
            onView.addSubview(spinnerView)
        }
        
        vSpinner = spinnerView
    }
    
    func removeSpinner() {
        DispatchQueue.main.async {
            vSpinner?.removeFromSuperview()
            vSpinner = nil
        }
    }
}

extension NSObject {
    var className: String {
        return String(describing: type(of: self))
    }
    
    class var className: String {
        return String(describing: self)
    }
}


extension UIApplication {
    
    var statusBarView: UIView? {
        return value(forKey: "statusBar") as? UIView
    }
    
    class func topViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        
        if let nav = base as? UINavigationController {
            return topViewController(base: nav.visibleViewController)
        }
        
        if let tab = base as? UITabBarController {
            let moreNavigationController = tab.moreNavigationController
            
            if let top = moreNavigationController.topViewController, top.view.window != nil {
                return topViewController(base: top)
            } else if let selected = tab.selectedViewController {
                return topViewController(base: selected)
            }
        }
        
        if let presented = base?.presentedViewController {
            return topViewController(base: presented)
        }
        
        return base
    }
}

extension Double {
    func clean() -> String {
        return self.truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.0f", self) : String(self)
    }
}

extension Float {
    func clean() -> String {
        return self.truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.0f", self) : String(self)
    }
    
    func rounded(toPlaces places:Int) -> Float {
        let divisor = pow(10.0, Float(places))
        return (self * divisor).rounded() / divisor
    }
}

extension UINavigationBar {
    func transparentNavigationBar() {
        self.setBackgroundImage(UIImage(), for: .default)
        self.shadowImage = UIImage()
        self.isTranslucent = true
    }
    
    func cancelTransparentNavigationBar() {
        self.setBackgroundImage(nil, for: UIBarMetrics.default)
        self.shadowImage = nil
        self.isTranslucent = false
    }
}

extension CGFloat {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> CGFloat {
        let divisor = pow(10.0, CGFloat(places))
        return (self * divisor).rounded() / divisor
    }
}





extension UIImageView {
    
    func rotate(){
        
        if UIApplication.shared.userInterfaceLayoutDirection == .rightToLeft {
            
            self.transform = self.transform.rotated(by: CGFloat(Float.pi))
        }
    }
}

public extension UISearchBar {
    
    public func setTextColorFont(color: UIColor, font: UIFont) {
        let svs = subviews.flatMap { $0.subviews }
        guard let tf = (svs.filter { $0 is UITextField }).first as? UITextField else { return }
        tf.textColor = color
        tf.font = font
    }
}

//Firebase
import FirebaseAuth
extension AuthErrorCode {
    var errorMessage: String {
        switch self {
        case .emailAlreadyInUse:
            return "The email is already in use with another account"
        case .userNotFound:
            return "Account not found for the specified user. Please check and try again"
        case .userDisabled:
            return "Your account has been disabled. Please contact support."
        case .invalidEmail, .invalidSender, .invalidRecipientEmail:
            return "Please enter a valid email"
        case .networkError:
            return "Network error. Please try again."
        case .weakPassword:
            return "Your password is too weak. The password must be 6 characters long or more."
        case .wrongPassword:
            return "Your password is incorrect. Please try again or use 'Forgot password' to reset your password"
        case .invalidCredential:
            return "Invalid Credential"
        case .userTokenExpired:
            return "you need to reauthenticate"
        default:
            return "Unknown error occurred"
        }
    }
}

extension UIViewController{
    func handleError(_ error: Error) {
        if let errorCode = AuthErrorCode(rawValue: error._code) {
            print(errorCode.errorMessage)
            let alert = UIAlertController(title: "Error", message: errorCode.errorMessage, preferredStyle: .alert)
            
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            
            alert.addAction(okAction)
            
            self.present(alert, animated: true, completion: nil)
            
        }
    }
    
}

