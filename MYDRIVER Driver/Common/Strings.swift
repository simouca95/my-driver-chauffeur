//
//  Strings.swift
//  MyDriver
//
//  Created by sami hazel on 6/19/19.
//  Copyright © 2019 MyDriverKw. All rights reserved.
//

import Foundation

let PRODUCTION_REF = "https://mydriverdb.firebaseio.com/";
let DEVELOPMENT_REF = "https://mydriverlab.firebaseio.com/";

let PRODUCTION_REF_APP = "https://mydriverdb.firebaseapp.com/";
let DEVELOPMENT_REF_APP = "https://mydriverlab.firebaseapp.com/";


let CURRENT_DB_REF = PRODUCTION_REF;
let CURRENT_DB_REF_APP = PRODUCTION_REF_APP;


