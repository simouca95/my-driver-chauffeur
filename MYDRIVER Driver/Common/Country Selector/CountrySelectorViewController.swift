//
//  CoutrySelectorViewController.swift
//  Exchange
//
//  Created by Lhioui Mahmoud on 6/22/17.
//  Copyright © 2017 Mahmoud. All rights reserved.
//

import UIKit
import STPopup

protocol CountrySelectorDelegate {
    func didSelectCountry(country: Country, viewController: UIViewController)
}

class CountrySelectorViewController: UITableViewController {
        
    var dataSource = NSMutableArray()
    var delegate: CountrySelectorDelegate!
    var selectedIndexPath : IndexPath!
    var fromSearch = false
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        let screenSize = UIScreen.main.bounds.size
        self.contentSizeInPopup = CGSize(width: screenSize.width - 60, height: screenSize.height - 160)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.popupController?.navigationBar.barTintColor = UIColor.darkGray
        self.popupController?.navigationBar.tintColor = UIColor.white
        self.popupController?.navigationBar.isTranslucent = false
//        self.popupController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white,NSAttributedStringKey.font: h2Regular!]
        
        self.popupController?.containerView.layer.cornerRadius = 10
        self.popupController?.containerView.clipsToBounds = true
        self.popupController?.hidesCloseButton = false
        self.tableView.register(UINib(nibName: "CountryViewCell", bundle: nil), forCellReuseIdentifier: "countryCell")
        let sortedArray = loadJSONCountries().sorted { ($0 as! Country).nameEn.localizedCaseInsensitiveCompare(($1 as! Country).nameEn) == .orderedAscending }
        self.dataSource = NSMutableArray(array: sortedArray)
        self.tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "countryCell", for: indexPath) as! CountryViewCell
        
        let country = dataSource[indexPath.row] as! Country
        
        if Languages.currentLanguage().starts(with: "ar") {
            
            cell.nameLabel.text = country.nameAr
        }
        else{
            
            cell.nameLabel.text = country.nameEn
        }
        
        cell.flagLabel.text = country.code.emojiFlag()
        
        if self.selectedIndexPath != nil && self.selectedIndexPath == indexPath{
            
            cell.backgroundColor = UIColor.lightGray.withAlphaComponent(0.2)
        }else{
            
            cell.backgroundColor = UIColor.white
        }
        return cell
    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.selectedIndexPath = indexPath
        self.tableView.reloadData()
        self.delegate.didSelectCountry(country: dataSource[indexPath.row] as! Country, viewController: self)
    }
    
}

