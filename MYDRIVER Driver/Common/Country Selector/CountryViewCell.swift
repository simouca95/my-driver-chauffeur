//
//  CountryViewCell.swift
//  Exchange
//
//  Created by Lhioui Mahmoud on 6/22/17.
//  Copyright © 2017 Mahmoud. All rights reserved.
//

import UIKit

class CountryViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var flagLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        nameLabel.text = ""
        flagLabel.text = ""
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func prepareForReuse() {
        nameLabel.text = ""
        flagLabel.text = ""
    }
}
