//
//  LoadingView.swift
//  Exchange
//
//  Created by Lhioui Mahmoud on 6/22/17.
//  Copyright © 2017 Mahmoud. All rights reserved.
//

import UIKit

class LoadingView: UIView {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var loadingLabel: UILabel!
    @IBOutlet weak var tryAgainButton: UIButton!
    var loadingMessage: String!
    var noDataMessage: String!
    var hideButton = true
    var hideMsg = true
    var isProcessing: Bool = false{
        didSet{
            if isProcessing{
                loadingLabel.text = "Loading"
                tryAgainButton.isHidden = true
                activityIndicator.startAnimating()
            }else{
                tryAgainButton.isHidden = hideButton
                loadingLabel.text = "noDataMessage"
                loadingLabel.isHidden = hideMsg
                activityIndicator.stopAnimating()
            }
        }
    }
    class func loadFromNibNamed(nibNamed: String, bundle : Bundle? = nil) -> LoadingView? {
        return UINib(nibName: nibNamed, bundle: bundle).instantiate(withOwner: nil, options: nil)[0] as? LoadingView
    }
}
